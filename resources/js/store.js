/*
* --------------------------------------------
* VUEX store.js
* --------------------------------------------
* Builds the data store from all of the modules for the whole application
*/

/*
*   Adds the promise polyfill for IE 11
*/
require( 'es6-promise' ).polyfill();

/*
*   Imports Vue & Vuex
*/
import Vue from 'vue';
import Vuex from 'vuex';

/*
*   Initialise Vuex on Vue
*/
Vue.use( Vuex );

/*
*   Imports all of the modules used in the application  to build the data store
*/
import { application } from './modules/application';

import { age } from './modules/age.js';

import { display } from './modules/display.js';

import { filters } from './modules/filters.js';

import { latestAds } from './modules/latest.js';

import { users } from './modules/users.js';

const vuexStore = new Vuex.Store({
    modules: {
        application,
        age,
        display,
        filters,
        latestAds,
        users
    }
});

/*
*   Export the data store
*/
export default vuexStore;
