/*
*   Imports the URL from the config.
*/
import { AM_CONFIG } from '../config.js';

/*
* Import the Event bus
*/
import { EventBus } from '../event-bus.js';

export default {
    /*
    *  GET   /api/v1/user - get the current authenticated user
    */
    getUser: function() {
        return axios.get( AM_CONFIG.API_URL + '/user' );
    },

    /*
    *  PUT  /api/v1/user - create or update a user
    */
    putUpdateUser: function( name, email, password ) {
        return axios.put( AM_CONFIG.API_URL + '/user',
            {
                name: name,
                email: email,
                password: password
            });
    },

    /*
    *  POST  /api/v1/user - register a new user
    */
    postUser: function( name, email, password ) {
        return axios.post( AM_CONFIG.API_URL + '/user',
            {
                name: name,
                email: email,
                password: password,
                _token: window.axios.defaults.headers.common['X-CSRF-TOKEN']
            });
    },

    /*
    *  POST  /api/v1/user/login - login a user
    */
    loginUser: function( email, password ) {
        return axios.post( AM_CONFIG.API_URL + '/user/login',
            {
                email: email,
                password: password,
                _token: window.axios.defaults.headers.common['X-CSRF-TOKEN']
            });
    },

    /*
    *   GET  /api/v2/user/logout/{uid}  - logs out the user
    */
    logoutUser: function( uid ) {
        return axios.get( AM_CONFIG.API_URL + '/user/logout/' + uid);
    },

    /*
    *  GET  /api/v1/user/fetch/{uid} - get the current authenticated user
    */
    fetchUser: function( uid ) {
        return axios.get( AM_CONFIG.API_URL + '/user/fetch/' + uid );
    },

    /*
    *  GET  /api/v1/user/email/{email} - Check | verify an email address
    */
    checkEmail: function( email ) {
        return axios.get( AM_CONFIG.API_URL + '/user/email/' + email );
    },

    /*
    *   GET  /api/v1/user/location - Get the users location from IpInfo
    */
    getUserLocation: function() {
        return axios.get( AM_CONFIG.API_URL + '/user/location' );
    },

    /*
    *   GET  /api/v1/user/states/{country} - Get the users states based on country code
    */
    getUserStates: function(country) {
        return axios.get( AM_CONFIG.API_URL + '/user/states/' + country);
    },

    /*
    *   GET  /api/v1/user/postcode/{country}/{state}/{suburb} - Get the users postcode based on country and suburb
    */
    getUserPostcode: function(country, state, suburb) {
        return axios.get( AM_CONFIG.API_URL + '/user/postcode/' + country + '/' + state + '/' + suburb);
    },

    /*
    *   GET  /api/v1/user/suburb/{country}/{state}/{postcode} - Get the users suburb based on country and postcode
    */
    getUserSuburb: function(country, state, postcode) {
        return axios.get( AM_CONFIG.API_URL + '/user/suburb/' + country + '/' + state + '/' + postcode);
    },

    /*
    *   GET /api/v1/user/area/{country}/{state}/{value} - Get the users area data (suburb & postcode) matching value
    */
    getUserArea: function(country, state, value) {
        return axios.get( AM_CONFIG.API_URL + '/user/area/' + country + '/' + state + '/' + value);
    },

    /*
    *   POST  /api/v1/upload/image/{context} - Upload a public image to the sandbox
    *
    *  Todo: !! Tried to handle this here - but ran into problems passing the status back to the view !!
    *        !! Opted to run this directly with the vue-component - worked like a charm !!
    *        Left for reference - remove later
    */
    uploadImage: function(formData, context, id = '') {
        return axios.post( AM_CONFIG.API_URL + '/upload/image/' + context + '/' + id,
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: function( progressEvent ) {
                    let uploadPercentage = parseInt( Math.round(( progressEvent.loaded / progressEvent.total ) * 100 ));

                    if (uploadPercentage === 100) {
                        clearInterval(window.uploadInterval);
                        setTimeout(function() {
                            EventBus.$emit('upload-progress', { value: 100 });
                        }, 200);
                    }

                    window.uploadInterval = setInterval(function() {
                        EventBus.$emit('upload-progress', { value: uploadPercentage });
                        // if (uploadPercentage >= 100) {
                        //     clearInterval(interval);
                        // }
                    }, 100);

                }.bind(this)
            }
        );
    },

    /*
     * POST  /api/v1/upload/image-delete/{context}/{id} - Remove an image from the current upload sand-box cache
     */
    removeImage: function(formData, context, id = '') {
        return axios.post( AM_CONFIG.API_URL + '/upload/image-delete/' + context + '/' + id,
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            }
        );
    }
}
