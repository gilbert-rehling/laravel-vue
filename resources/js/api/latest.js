/*
*   Import the application JS config
*/
import { AM_CONFIG } from "../config";

export default {
    /*
    *   Get all ads
    *   GET  /api/v1/latest
    */
    getAds: function() {

        console.log("fetching ads: " + AM_CONFIG.API_URL + '/latest');

        return axios.get( AM_CONFIG.API_URL + '/latest' );
    },

    /*
    *   Get a single ad
    *   GET  /api/vi/latest/{slug}
    */
    getAd: function( slug ) {
        return axios.get( AM_CONFIG.API_URL + '/latest/' + slug );
    },

    /*
    *   Get a single ad
    *   GET  /api/vi/latest/{id}
    */
    getNewAd: function( id ) {
        return axios.get( AM_CONFIG.API_URL + '/latest/ad/' + id );
    },

    /*
    *   Get the ad sections
    *   GET  /api/vi/sections
    */
    getSections: function() {
        return axios.get( AM_CONFIG.API_URL + '/sections' );
    },

    /*
    *   Get the ad categories
    *   GET  /api/vi/categories
    */
    getCategories: function() {
        return axios.get( AM_CONFIG.API_URL + '/categories' );
    },

    /*
    *   Get the ad sub categories for selected category
    *   GET  /api/vi/categories/{parent}
    */
    getSubCategories: function( parent ) {
        return axios.get( AM_CONFIG.API_URL + '/categories/' + parent );
    },

    /*
     * POST  /api/js/latest/post/public - Handles sending a 'public/anonymous' ad post
     */
    publicPost: function( formData ) {
        return axios.post( AM_CONFIG.API_URL + '/latest/post/public',
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            }
        )
    }
}
