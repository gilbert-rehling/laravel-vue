/*
*   routes.js
*
*   Contains all routes for this application
*/

/*
*    Import Vue and VueRouter to extend the routes
*/
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store.js'

/*
*   Extends Vue to use VueRouter
*/
Vue.use( VueRouter );

/*
*   Create a VueRouter that is used to handle all the application routes
*/
let router = new VueRouter({
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0}
    },
    routes: [
        {
            path: '/',
            name: 'layout',
            redirect: { name: 'latest' },
            component: Vue.component( 'Layout', require( './layouts/Layout.vue' ).default ),
            children: [
                {
                    path: 'latest',
                    name: 'latest',
                    component: Vue.component( 'Latest', require( './pages/Latest.vue').default )
                },
                {
                    path: 'age',
                    name: 'age',
                    component: Vue.component( 'Age', require( './pages/Age.vue').default )
                },
                {
                    path: 'about',
                    name: 'about',
                    component: Vue.component( 'About', require( './pages/About.vue').default )
                },
                {
                    path: 'about-us',
                    name: 'about-us',
                    component: Vue.component( 'About', require( './pages/About.vue').default )
                },
                {
                    path: 'contact',
                    name: 'contact',
                    component: Vue.component( 'Contact', require( './pages/Contact.vue').default )
                },
                {
                    path: 'contact-us',
                    name: 'contact-us',
                    component: Vue.component( 'Contact', require( './pages/Contact.vue').default )
                },
                {
                    path: 'post',
                    name: 'post',
                    component: Vue.component( 'Post', require( './pages/Post.vue').default )
                },
                {
                    path: 'privacy',
                    name: 'privacy',
                    component: Vue.component( 'Privacy', require( './pages/Privacy.vue').default )
                },
                {
                    path: 'privacy-policy',
                    name: 'privacy-policy',
                    component: Vue.component( 'Privacy', require( './pages/Privacy.vue').default )
                },
                {
                    path: 'terms',
                    name: 'terms',
                    component: Vue.component( 'Terms', require( './pages/Terms.vue').default )
                },
                {
                    path: 'terms-and-conditions',
                    name: 'terms-and-conditions',
                    component: Vue.component( 'Terms', require( './pages/Terms.vue').default )
                },
            ]
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            redirect: { name: 'ads' },
            component: Vue.component( 'Admin', require('./layouts/Members.vue' ) ),
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: 'ads',
                    name: 'ads',
                    component: Vue.component( 'Ads', require( './pages/members/Ads.vue').default ),
                    meta: {
                        requiresAuth: true
                    }
                }
            ]
        }
    ]
});

/*
 * For now - a simplified Auth middleware function
 */
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next();
            return;
        }
        next('/#/login');

    } else {
        next();
    }
});

export default router;
