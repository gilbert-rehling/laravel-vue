export const LatestTextFilter = {
  methods: {
    processLatestTextFilter( ad, text ) {
      /*
      *  Only process if the text is greater than 1
      */
      if ( text.length > 1 ) {
        /*
        *  If the title, intro, text, suburb or postcodes matches the entered text return true otherwise return false.
        */
        if( ad.title.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
          || ad.intro.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
          || ad.text.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
          || ad.phone.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
          || ad.suburb.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
          || ad.postcodes.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )) {
            // ad is matched
          return true;

        } else {
            // ad is not matched
          return false;
        }

      } else {
          // error!! search text not founds
        return true;
      }
    }
  }
};
