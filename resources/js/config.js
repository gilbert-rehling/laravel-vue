/*
*   Define the API route we will be using
*/
let api_url = '',
    web_url = '';

/*
*   Set the API route during the build process
*/

console.log(process.env.NODE_ENV);

switch ( process.env.NODE_ENV )  {
    case 'development':
    case 'dev':
    case 'local':
        api_url = '//am.local/api/v1';
        web_url = '//am.local';
        break;

    case 'staging':
        api_url = '//staging.am.com/api/v1';
        web_url = '//staging.am.com';
        break;

    case 'production':
        api_url = '//www.am.com/api/v1';
        web_url = '//www.am.com';
        break;
}

export const AM_CONFIG = {
    API_URL: api_url,
    WEB_URL: web_url,
    SITE_NAME: 'AM',
    SITE_FULLNAME: 'AM Classifieds',
    LANGUAGES: { en: 'English', zh: 'Chinese' }
};
