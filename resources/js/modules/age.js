/*
* ----------------------------------------------------
* VUEX modules/age.js
* ----------------------------------------------------
* The Vuex data store for age views
*/
export const age = {
    /*
    *   Defines the 'state' being monitored for the module
    */
    state: {
        age: []
    },

    actions: {
        //
    },

    mutations: {
        //
    },

    getters: {
        //
    }
};
