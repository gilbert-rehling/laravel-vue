@component('mail::message')
    # New public post

    Hey there,
              You recently posted a new ad on the AdultMuse Classifieds on: {{ $date }}

    The following links will allow you view or edit your ad.
    Please consider creating a 'member account' -- as a member you have full access to the members ad management console.

    @component('mail::button', ['url' => "{{ $url }}/latest/view/{{ $user_key }}/{{ $post_key }}"])
        View Your Ad Here
    @endcomponent

    @component('mail::button', ['url' => "{{ $url }}/latest/edit/{{ $user_key }}/{{ $post_key }}"])
        Edit Your Ad Here
    @endcomponent

@endcomponent
