<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| These are the current web routes for the AM application
|
*/

/*
| Localization - customised to provide language files data to Vue
*/
Route::get('/js/lang.js', function() {
    $strings = Cache::rememberForever( 'lang.js', function() {

        // output container
        $strings = [];

        // defined the languages we need to load
        $languages = array('en', 'zh');
        //$lang = config('app.locale');

        foreach ($languages as $lang) {
            // fetch the files for this language set
            $files = glob( resource_path('lang/' . $lang) . '/*.php');
            foreach ($files as $file) {
                $name  = basename( $file, '.php');
                $strings[ $lang ][ $name ] = require $file;
            }
        }

        // output the combined language set
        return $strings;
    });


    // this needs to support chinese chars
    echo('window.i18n = ' . json_encode( $strings, JSON_UNESCAPED_UNICODE ) . ';');
    exit();

})->name('assets-lang');

/*
| Localisation assistance - getting the accept language value from the browser
*/
Route::get('/js/acceptLang.js', function() {
    //dd($_SERVER);
    header('Content-Type: text/javascript');
    $string = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    echo explode(";", explode(",", $string)[1])[0];
    exit();
});

/*
|   Default public route - requires Age confirmation Cookie
*/
Route::get( '/', 'Web\AppController@getApp');

/*
| Path to specialised image loader
*/
Route::get('/image/loader/{context}/{image}/{w?}/{h?}', 'Web\ImageLoaderController@index')
    ->middleware('guest');

/*
| Age confirmation form loader
*/
Route::get('/age', 'Web\AgeController@index')->name('age')
    ->middleware('guest');
/*
| Age confirmation action method
*/
Route::post('/age/confirm', 'Web\AgeController@confirm')->name('confirmation')
    ->middleware('guest');

/*
| Social logins
*/
Route::post('/login', 'Auth\LoginController@login');

Route::get('/login/form', 'Web\AppController@getLoginForm')->name('login-form')
    ->middleware('guest');

Route::get('/login/{social}', 'Web\AuthenticationController@getSocialRedirect')
    ->middleware('guest');

Route::get('/login/{social}/callback', 'Web\AuthenticationController@getSocialCallback')
    ->middleware('guest');

/*
| Logout
*/
Route::get('/logout', 'Web\AppController@getLogout')
    ->name('logout');

/* Redundant */
Route::get('/dashboard', 'Web\HomeController@index')->name('dashboard');

Auth::routes();
