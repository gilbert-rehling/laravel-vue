<?php

use Illuminate\Http\Request;

// This is the public route group
Route::group(['prefix' => 'v1'], function() {
    /*
    |-------------------------------------------------------------------------------
    | Register (POST) a User's Profile
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Controller:     Api\UsersController@postRegisterUser
    | Method:         POST
    | Description:    Registers a user's profile
    */
    Route::post('/user', 'Api\UsersController@postRegisterUser');

    /*
    |-------------------------------------------------------------------------------
    | Login (POST) a User's Credentials for authentication
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/login
    | Controller:     Auth\LoginController@login
    | Method:         POST
    | Description:    Login a user using an AJAX post
    */
    Route::post('/user/login', 'Auth\LoginController@login');

    /*
    |-------------------------------------------------------------------------------
    | Logout (GET) a User
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/logout/{uid}
    | Controller:     Auth\LoginController@logout
    | Method:         POST
    | Description:    Login a user using an AJAX post
    */
    Route::get('/user/logout/{uid}', 'Auth\LoginController@logout');

    /*
    |-------------------------------------------------------------------------------
    | Check (GET) an email address
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/email/{email}
    | Controller:     API\UsersController@checkEmail;
    | Method:         GET
    | Description:    Checks application DB for an email during the registration process
    */
    Route::get('/user/email/{email}', 'Api\UsersController@checkEmail');

    /*
    |-------------------------------------------------------------------------------
    | Get (GET) the users location data
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/location
    | Controller:     Api\UsersController@getUserLocation
    | Method:         GET
    | Description:    Uses the IpInfo service to get the visitors location data
    */
    Route::get('/user/location', 'Api\UsersController@getUserLocation');

    /*
    |-------------------------------------------------------------------------------
    | Get (GET) the users states for their country (only AU and USA available)
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/states/{country}
    | Controller:     Api\UsersController@getUserStates
    | Method:         GET
    | Description:    Get the states for the user's country
    */
    Route::get('/user/states/{country}', 'Api\UsersController@getUserStates');

    /*
    |-------------------------------------------------------------------------------
    | Get (GET) the postcode for the suburb provided (only AU and NZ)
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/postcode/{country}/{state}/{suburb}
    | Controller:     Api\UsersController@getUserStates
    | Method:         GET
    | Description:    Get the postcode matching the suburb
    */
    Route::get('/user/postcode/{country}/{state}/{suburb}', 'Api\UsersController@getUserPostcode');

    /*
    |-------------------------------------------------------------------------------
    | Get (GET) the suburb(s) for the postcode provided (only AU and NZ)
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/suburb/{country}/{state}/{postcode}
    | Controller:     Api\UsersController@getUserStates
    | Method:         GET
    | Description:    Get the suburbs matching the postcode
    */
    Route::get('/user/suburb/{country}/{state}/{postcode}', 'Api\UsersController@getUserSuburb');

    /*
    |-------------------------------------------------------------------------------
    | Get (GET) the area data using the value provided (only AU and NZ)
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user/area/{country}/{state}/{value}
    | Controller:     Api\UsersController@getUserArea
    | Method:         GET
    | Description:    Get the suburbs + postcode matching the value
    */
    Route::get('/user/area/{country}/{state}/{value}', 'Api\UsersController@getUserArea');

    /*
    * -------------------------------------------------------
    * Get all Latest Ads (public route)
    * -------------------------------------------------------
    * URL:         /api/v1/latest
    * Controller:  API/PublicController@getAds
    * Method:      GET
    * Description: Gets all the latest ads including premium ads
    */
    Route::get('/latest', 'Api\PublicController@getAds');

    /*
    * -------------------------------------------------------
    * Get a single Ads (public route)
    * -------------------------------------------------------
    * URL:         /api/v1/latest/{slug}
    * Controller:  Api/PublicController@getAd
    * Method:      GET
    * Description: Gets a single ad
    */
    Route::get('/latest/{slug}', 'Api\PublicController@getAd');

    /*
    * -------------------------------------------------------
    * Get a single Ads - usually a new post (public route)
    * -------------------------------------------------------
    * URL:         /api/v1/latest/ad/{id}
    * Controller:  Api/PublicController@getNewAd
    * Method:      GET
    * Description: Gets a single ad
    */
    Route::get('/latest/ad/{id}', 'Api\PublicController@getNewAd');

    /*
    * -------------------------------------------------------
    * Get all Sections to use as options
    * -------------------------------------------------------
    * URL:         /api/v1/sections
    * Controller:  Api/PublicController@getSections
    * Method:      GET
    * Description: Gets all sections
    */
    Route::get('/sections', 'Api\PublicController@getSections');

    /*
    * -------------------------------------------------------
    * Get all Categories to use as options
    * -------------------------------------------------------
    * URL:         /api/v1/categories
    * Controller:  Api/PublicController@getCategories
    * Method:      GET
    * Description: Gets all categories
    */
    Route::get('/categories', 'Api\PublicController@getCategories');

    /*
    * -------------------------------------------------------
    * Get all Sub Categories to use as options
    * -------------------------------------------------------
    * URL:         /api/v1/categories/{parent}
    * Controller:  Api/PublicController@getSubCategories
    * Method:      GET
    * Description: Gets all categories
    */
    Route::get('/categories/{parent}', 'Api\PublicController@getSubCategories');

    /*
    * -------------------------------------------------------
    * Post (POST) upload a sandboxed image for the public ad placement form
    * -------------------------------------------------------
    * URL:         /api/v1/upload/{function}/{context?}/{id?}
    * Controller:  Api/FilesController@uploadImage
    * Method:      POST
    * Description: Upload an image
    */
    Route::post('/upload/{function}/{context}/{id?}', 'Api\FilesController@index');

    /*
    * -------------------------------------------------------
    * Post (POST) delete an image (photo)
    * -------------------------------------------------------
    * URL:         /api/v1/upload/{function}/{context?}/{id?}
    * Controller:  Api/FilesController@uploadImage
    * Method:      POST
    * Description: Upload an image
    */
    Route::post('/upload/{function}/{context}/{id?}', 'Api\FilesController@index');

    /*
    * -------------------------------------------------------
    * Post (POST) a public ad
    * -------------------------------------------------------
    * URL:         /api/v1/latest/post/public
    * Controller:  Api/PublicPostController@index
    * Method:      POST
    * Description: Post a public add (not via the members area)
    */
    Route::post('/latest/post/public', 'Api\PublicPostController@index');

});

// This is the Auth route group
Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function() {

    /*
    |-------------------------------------------------------------------------------
    | Get User
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Controller:     Api\UsersController@getUser
    | Method:         GET
    | Description:    Gets the authenticated user
    */
    Route::get('/user', 'Api\UsersController@getUser');

    /*
    |-------------------------------------------------------------------------------
    | Updates a User's Profile
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Controller:     Api\UsersController@putUpdateUser
    | Method:         PUT
    | Description:    Updates the authenticated user's profile
    */
    Route::put('/user', 'Api\UsersController@putUpdateUser');

});
