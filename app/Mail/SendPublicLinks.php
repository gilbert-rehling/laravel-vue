<?php

namespace App\Mail;

/**
 * @uses
 */
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendPublicLinks
 * Sends an access key to the ad poster allowing direct access to the ad they created
 *
 * @package App\Mail
 */
class SendPublicLinks extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user key for public posting.
     *
     * @var $user_key
     */
    protected $user_key;

    /**
     * The post key from the posted ad
     *
     * @var $post_key
     */
    protected $post_key;

    /**
     * The user instance.
     *
     * @var $user object
     */
    protected $user;

    /**
     * The date for this message.
     *
     * @var $date
     */
    protected $date;

    /**
     * The current host name.
     *
     * @var $host
     */
    protected $host;

    /**
     * Create a new message instance.
     * SendKey constructor.
     *
     * @param $userKey
     * @param $postKey
     */
    public function __construct($userKey, $postKey)
    {
        $this->user_key = $userKey;
        $this->post_key = $postKey;
        $this->date     = date("d:m:Y h:i:s", time());
        $this->host     = config('url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.key.sendlinks')
            ->with([
                'user_key' => $this->user_key,
                'post_key' => $this->post_key,
                'date'     => $this->date,
                'url'      => $this->host
            ]);
    }
}
