<?php

namespace App\Http\Middleware;

/**
 * @uses
 */
use Closure;

/**
 * Class AgeConfirmation
 *
 * @package App\Http\Middleware
 */
class AgeConfirmation
{
    /**
     * Monitor an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->get('ageConfirmation', null)) {
            $ageConfirmation = $request->cookie('ageConfirmation');
            if ( !isset($ageConfirmation) || strlen($ageConfirmation) < 10) {
                return redirect()->route('age');
            }
            $request->session()->put('ageConfirmation', $ageConfirmation);
        }
        return $next($request);
    }
}
