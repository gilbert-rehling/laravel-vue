<?php

namespace App\Http\Middleware;

/**
 * @uses
 */
use Closure;

/**
 * Class PublicMiddleware
 *
 * @package App\Http\Middleware
 */
class PublicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
