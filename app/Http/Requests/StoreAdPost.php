<?php

namespace App\Http\Requests;

/**
 * @uses
 */
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreAdPost
 *
 * @package App\Http\Requests
 */
class StoreAdPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set the validation rules that apply to the ad post request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ageBox'         => 'required|numeric',
            'categoryBox'    => 'required|numeric',
            'countryBox'     => 'required|string|max:2',
            'date'           => 'required_if:publishedBox,2|string',
            'imagesBox'      => 'nullable|string',
            'introBox'       => 'required|string|max:200',
            'languageBox'    => 'required|string|max:2',
            'phoneBox'       => 'required|numeric',
            'postcodeBox'    => 'required|numeric|max:9999',
            'postcodesBox'   => 'required|string',
            'publicEmail'    => 'required_if:publicPost,true|email:rfc,dns',
            'publicPhone'    => 'required_if:publicPost,true|numeric',
            'publicPost'     => 'sometimes|accepted',
            'publishedBox'   => 'required|numeric|max:2',
            'stateBox'       => 'required|string|max:3',
            'suburbBox'      => 'required|string',
            'sectionBox'     => 'required|numeric',
            'send'           => 'required|boolean',
            'subCategoryBox' => 'nullable|numeric',
            'textBox'        => 'required|string|max:1000',
            'titleBox'       => 'required|string|max:150',
            'websiteBox'     => 'sometimes|url',
            'userPublicKey'  => 'sometimes|string'
        ];
    }

    /**
     * Set the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'ageBox'        => 'A age was not selected',
            'categoryBox'   => 'A category was not selected',
            'countryBox'    => 'A country was not selected',
            'date'          => 'A valid publish date was not found',
            'introBox'      => 'The intro text is invalid',
            'languageBox'   => 'A language was not selected',
            'phoneBox'      => 'The ads phone number is invalid',
            'postcodeBox'   => 'The postcode is invalid',
            'postcodesBox'  => 'Valid area data was not provided',
            'publicEmail'   => 'The public email address was not provided',
            'publicPhone'   => 'The public phone was not provided',
            'publicPost'    => 'The public post notification was invalid',
            'publishedBox'  => 'A published status was not selected',
            'stateBox'      => 'The state value is invalid',
            'suburbBox'     => 'The suburb value is invalid',
            'sectionBox'    => 'A section was not selected',
            'send'          => 'The send verification was invalid',
            'subCategoryBox' => 'Invalid sub category provided',
            'textBox'       => 'The ad text is invalid',
            'titleBox'      => 'The ad title is invalid',
            'websiteBox'    => 'The provided URL was invalid',
            'userPublicKey' => 'the userPublicKey value is invalid'
        ];
    }
}
