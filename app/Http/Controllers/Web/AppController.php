<?php

namespace App\Http\Controllers\Web;

/**
 * @uses
 */
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * Class AppController
 *
 * @package App\Http\Controllers\Web
 */
class AppController extends Controller
{
    public function getApp() {
        return view('layouts.app');
    }

    public function getLogin() {
        return view('public.login');
    }

    public function getLoginForm() {
        return view('public.login');
    }

    public function getLogout() {
        Auth::logout();
        return redirect('/login' );
    }
}
