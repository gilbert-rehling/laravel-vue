<?php

namespace App\Http\Controllers\Web;

/**
 * @uses
 */
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ipinfo\ipinfo\IPinfo;
use App\Http\Controllers\Controller;

/**
 * Class PublicController
 *
 * These methods were originally created to handle web based initialisation of the project
 *
 * @package App\Http\Controllers\Web
 */
class PublicController extends Controller
{
    /**
     * @var null|string $slug
     */
    private $slug = null;

    /**
     * @var int
     */
    private $limit = 30;

    /**
     * States array for Australia
     *
     * @var array $states
     */
    private $states = array(
        "New South Wales" => "NSW",
        "Queensland" => "QLD",
        "Northern Territory" => "NT",
        "Western Australia" => "WA",
        "South Australia" => "SA",
        "Victoria" => "VIC",
        "Australian Capitol Territory" => "ACT",
        "Tasmania" => "TAS"
    );

    /**
     * @var \ipinfo\ipinfo\Details|null
     */
    private $ipinfo = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('age');
    }

    /**
     * Test the User Agent for origin
     *
     * @param Request $request
     * @return string
     */
    private function setBotStatus(Request $request) {
        $userAgent = $request->header('User-Agent');
        $r = preg_match('/bot|spider/', $userAgent);
        if ($r) {
            return '';
        }
        return 'u-display-none is-hidden';
    }

    /**
     * @return \ipinfo\ipinfo\Details
     * @throws \ipinfo\ipinfo\IPinfoException
     */
    private function getIPInfo()
    {
        try {
            // ToDo: !! this is local DEV mode !! change before deployment !!
            $ip = '14.202.88.95'; //  $_SERVER['REMOTE_ADDR']
            $access_token = 'e5f368ed86097c';
            $client       = new IPinfo($access_token);
            return        $client->getDetails($ip);

        } catch(\Exception $e) {
            throw new $e;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param string $function
     * @param null $id
     * @param null $slug
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $function = 'getLatestAdsList', $id = null, $slug = null)
    {
        $this->slug = (isset($slug) ? $slug : (isset($id) ? $id : false));
        try {
            $this->ipinfo = $this->getIPInfo();

        } catch(\Exception $e) {
            // silent failure
        }

        // get the main query result
        $output  = $this->runSwitch($function, $request);

        // always load the premium ads
        $premium = $this->getPremiumAds();

        // get the User Agent status
        $isABot = $this->setBotStatus( $request );

        return view('public.directory', compact( 'output','premium', 'isABot'));

    }

    /**
     * Run a method based on the provide function name
     *
     * @param $function
     * @param $request
     *
     * @return array
     */
    private function runSwitch($function, $request) {
        switch ($function) {
            case 'getLatestAdsList':
                $this->request = $request;
                return $this->getLatest(false, false);
                break;

            case 'getPremiumAdsList':
                $this->request = $request;
                return $this->getPremiumAds();
                break;

            case 'getSlug':
                return $this->getSlug($request);
                break;

            case 'getCategoryCount':
                return $this->getCategoryCount();
                break;

            case 'getCategoryList';
                return $this->getCategoryList();
                break;

            case 'claimAd':
                return $this->claimAd($request);
                break;

            case 'sendReset':
                return $this->sendReset($request);
                break;

        }
    }

    /**
     * @param $out
     * @param $in
     * @param $var
     *
     * @return mixed
     */
    private function stringReplace($out, $in, $var) {
        $var = str_replace('"', '', $var);
        return str_replace($out, $in, $var);
    }

    /**
     * @param $slug
     * @param $context
     *
     * @return array
     */
    private function getLatest($slug, $context) {

        if ($slug && $context) {
            switch($context) {
                case "section":
                    $query = "SELECT * FROM adListings al
                        LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                        LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                        LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                        WHERE al.published = 1 AND al.section_id = ?
                        ORDER BY (al.ordering_multiplier * al.ordering_value) DESC
                        LIMIT $this->limit";
                    $results = DB::select( $query, [ $slug ] );
                    break;

                case "category":
                    $query = "SELECT * FROM adListings al
                        LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                        LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                        LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                        WHERE al.published = 1 AND al.category_sub_id = ?
                        ORDER BY (al.ordering_multiplier * al.ordering_value) DESC
                        LIMIT $this->limit";
                    $results = DB::select( $query, [ $slug ] );
                    break;

                case "sub":
                    $query = "SELECT * FROM adListings al
                        LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                        LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                        LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                        WHERE al.published = 1 AND al.category_id = ?
                        ORDER BY (al.ordering_multiplier * al.ordering_value) DESC
                        LIMIT $this->limit";
                    $results = DB::select( $query, [ $slug ] );
                    break;
            }

        } else {
            $language = $this->request->input('language', 'en');
            $country  = $this->request->input('country', 'AU');
            $state    = $this->request->input('state', $this->ipinfo->region);
            $suburb   = $this->request->input('suburb', $this->ipinfo->city);
            $postcode = $this->request->input('postcode', $this->ipinfo->postal);

            if ($country == 'AU') {
                // set the state for Db compatability
                $state = $this->states[$state];
            }

            if ($country && $suburb && $postcode && $state) {
                $this->limit = 100;
                $query = "SELECT * FROM adListings al
                    LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                    LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                    LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                    WHERE al.published = 1 AND al.country = ? AND al.state = ? AND al.language = ?
                    ORDER BY (al.suburb = ? OR al.postcode = ?) DESC, (al.ordering_value * al.ordering_multiplier) DESC, al.ad_id DESC
                    LIMIT $this->limit";

                $results = DB::select( $query, [ $country, $state, $language, $suburb, $postcode ] );

            } else {
                $this->limit = 250;
                $query = "SELECT * FROM adListings al
                    LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                    LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                    LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                    WHERE al.published = 1 AND al.country = ? AND al.state = ? AND al.language = ?
                    ORDER BY (al.ordering_value * al.ordering_multiplier) DESC
                    LIMIT $this->limit";
                $results = DB::select( $query, [ $country, $state, $language ] );
            }
        }

        $ids = array();
        if ($results) {
            $introOut = array("&#44;","&#39;","<br>","<br />","\"");
            $introIn  = array(",","''","","","");
            $out      = array("&#44;","&#39;","\"");
            $in       = array(",","'","");
            $output   = array();
            foreach ($results as $row) {
                $ids[]      = $row->ad_id;
                $created    = date("d-m-Y h:i:s", strtotime($row->ad_created_at));
                $date       = date("d-m-Y", strtotime($row->ad_created_at));
                $arr = array(
                    'id'              => $row->ad_id,
                    'title'           => $this->stringReplace($out, $in, $row->title),
                    'intro'           => $this->stringReplace($introOut, $introIn, $row->intro),
                    'text'            => $this->stringReplace($out, $in, $row->text),
                    'images'          => $row->images,
                    'language'        => $row->language,
                    'age'             => $row->age,
                    'phone'           => $row->phone,
                    'postcodes'       => $row->postcodes,
                    'suburb'          => $row->suburb,
                    'state'           => $row->state,
                    'userId'          => $row->user_id,
                    'rating'          => ((isset($row->advertiser_rating)) ? $row->advertiser_rating : 50),
                    'image_rating'    => ((isset($row->image_rating)) ? $row->image_rating : 0),
                    'views_list'      => $row->total_views_list,
                    'views_ad'        => $row->total_views_ad,
                    'slug_ad'         => $row->slug_ad,
                    'slug_section'    => $row->slug_section,
                    'slug_category'   => $row->slug_category,
                    'published'       => $row->published,
                    'ordering'        => ($row->ordering_multiplier * $row->ordering_value),
                    'created'         => $created,
                    'date'            => $date,
                    'updated'         => $row->ad_updated_at
                );
                $output[] = $arr;
            }
            $sql  = "UPDATE adListings SET total_views_list = total_views_list+1 WHERE ad_id IN (" . implode(",",$ids) . ")";
            DB::statement( $sql );

            //echo '</pre>'; var_dump($output); echo '</pre>'; die;
            //dd($output);

            return $output;
        }
    }

    /**
     * @return array
     */
    private function getPremiumAds() {
        $language = $this->request->input('language', 'en');
        $country  = $this->request->input('country', 'AU');
        $state    = $this->request->input('state', $this->ipinfo->region);
        $suburb   = $this->request->input('suburb', $this->ipinfo->city);
        $postcode = $this->request->input('postcode', $this->ipinfo->postal);

        if ($country && $suburb && $postcode) {
            $query = "SELECT * FROM adListings al
                LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                WHERE al.published = 1 AND al.premium = 1 AND al.country = ? AND al.language = ?
                ORDER BY (al.suburb = ? OR al.postcode = ?) DESC, (al.ordering_value * al.ordering_multiplier) DESC
                LIMIT 50";
            $results = DB::select( $query, [ $country, $language, $suburb, $postcode ] );

        } else {
            $query = "SELECT * FROM adListings al
                LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                WHERE al.published = 1 AND al.premium = 1 AND al.country = ? AND al.language = ?
                ORDER BY (al.ordering_value * al.ordering_multiplier) DESC
                LIMIT 50";
            $results = DB::select( $query, [ $country, $language ] );
        }
        $ids    = array();
        $output = array();
        if ($results) {
            $introOut = array("&#44;","&#39;","<br>","<br />");
            $out      = array("&#44;","&#39;");
            $in       = array(",","'");
            foreach ($results as $row) {
                $ids[]                = $row->ad_id;
                $images = explode(",", $row->images);
                $img    = $images[0];
                $web    = strlen($row->website) ? $row->website : 'javascript:void(0)';
                $arr = array(
                    'id'              => $row->ad_id,
                    'title'           => $this->stringReplace($out, $in, $row->title),
                    'intro'           => $this->stringReplace($introOut, $in, $row->intro),
                    'text'            => $this->stringReplace($out, $in, $row->text),
                    'images'          => $img,
                    'language'        => $row->language,
                    'age'             => $row->age,
                    'phone'           => $row->phone,
                    'postcodes'       => $row->postcodes,
                    'suburb'          => $row->suburb,
                    'state'           => $row->state,
                    'userId'          => $row->user_id,
                    'rating'          => ((isset($row->advertiser_rating)) ? $row->advertiser_rating : 50),
                    'image_rating'    => ((isset($row->image_rating)) ? $row->image_rating : 0),
                    'views_list'      => $row->total_views_list,
                    'views_ad'        => $row->total_views_ad,
                    'slug_ad'         => $row->slug_ad,
                    'slug_section'    => $row->slug_section,
                    'slug_category'   => $row->slug_category,
                    'published'       => $row->published,
                    'website'         => $web,
                    'ordering'        => ($row->ordering_multiplier * $row->ordering_value),
                    'created'         => $row->ad_created_at,
                    'updated'         => $row->ad_updated_at
                );
                $output[] = $arr;
            }
            $sql  = "UPDATE adListings SET total_views_list = total_views_list+1 WHERE ad_id IN (" . implode(",",$ids) . ")";
            DB::statement( $sql );
        }
        return $output;
    }
}
