<?php

namespace App\Http\Controllers\Web;

/**
 * @uses
 */
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
//use Image;

/**
 * Class ImageLoaderController
 *
 * @package App\Http\Controllers\Web
 */
class ImageLoaderController extends Controller
{
    /**
     * Run the image loader - use the $context value passed in the URI to determine the source/destination
     */
    public function index(Request $request)
    {
        $context = $request->route('context');
        $image   = $request->route('image');
        $width   = $request->route('w', null);
        $height  = $request->route('h', null);

     //   dd($context);

        if ($image) {
            switch ( $context ) {
                case 'sandbox':
                    return $this->loadSandbox( $image, $width, $height );
                    break;

                case 'photo':
                    return $this->loadPhoto( $image, $width, $height );
                    break;

                case 'image':
                    return $this->loadImage( $image, $width, $height );
                    break;

                default:
                    $img = Image::canvas(100, 100, '#ff0000');
                    return $img->response();
                    break;
            }

        } else {
            $img = Image::canvas(100, 100, '#ff0000');
            return $img->response();
        }
    }

    /**
     * Load an image from the sandbox for public safety
     *
     * @param $image
     * @param $width
     * @param $height
     *
     * @return mixed|string
     */
    public function loadSandbox($image, $width, $height)
    {
        if (Storage::disk('sandbox')->exists('/images/' . $image)) {

            // set the path
            $imagePath = storage_path() . '/sandbox/images/' . $image;

            // set the size if provided
            if ($width && $height) {
                $img = Image::make($imagePath);
                $img = $img->resize($width, $height);
                $img->orientate();
                return $img->response('jpg');

            } else {
                return Image::make($image)->orientate()->response('jpg');
            }
        }
        return '';
    }

    /**
     * To be implemented
     *
     * @param $image
     * @param $width
     * @param $height
     * @return string
     */
    public function loadPhoto($image, $width, $height)
    {
        return '';
    }

    /**
     * To be implemented
     *
     * @param $image
     * @param $width
     * @param $height
     * @return string
     */
    public function loadImage($image, $width, $height)
    {
        return '';
    }

}
