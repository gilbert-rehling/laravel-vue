<?php

namespace App\Http\Controllers\Web;

/**
 * @uses
 */
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Web
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }

    /**
     * Load the App template
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getApp()
    {
        return view('app');
    }

    /**
     * Show the Login template
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('login');
    }
}
