<?php

namespace App\Http\Controllers\Web;

/**
 * @uses
 */
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

/**
 * Class AgeController
 *
 * @package App\Http\Controllers\Web
 */
class AgeController extends Controller
{
    /**
     * Show the age declaration form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //return view('public.age');
        return view('layouts.app');
    }

    /**
     * Accept and set an age confirmation
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm( Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();

            // its fundamental stuff
            if ($data['iamanadult'] == 'yes' && $data['enter'] == 'now') {
                // get the hash
                $output = $this->getAgeConfirmation( $request );
                $hash     = $output['key'];
                $llct     = time()+60*60*24*180;
                Cookie::queue('ageConfirmation', $hash, $llct);

                // send them home
                return redirect()->route('public');
            }
        }
        return redirect()->route('age');
    }

    /**
     * Method used to generate Age Confirmation key (stored as a cookie)
     *
     * @param $request
     *
     * @return array
     */
    private function getAgeConfirmation(Request $request) {
        $browser    = $_SERVER['HTTP_USER_AGENT'];
        $remoteIp   = $_SERVER['REMOTE_ADDR'];
        $time       = time();
        $hash       = hash('sha1', $browser.$time.$remoteIp);
        $request->session()->put('ageConfirmation', $hash);
        // return result
        return array("key" => $hash, "sid" => $request->session()->get('id'));
    }
}
