<?php

namespace App\Http\Controllers\Web;

/**
 * @uses
 */
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Laravel\Socialite\Facades\Socialite;

/**
 * Class AuthenticationController
 *
 * @package App\Http\Controllers\Web
 */
class AuthenticationController extends Controller
{
    /**
     * Create the social redirect
     *
     * @param $account
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getSocialRedirect( $account )
    {
        try {
            return Socialite::with( $account )->stateless()->redirect();

        } catch (\InvalidArgumentException $e) {
            return redirect('/#/login');
        }
    }

    /**
     * Handle the social redirect result
     *
     * @param $account
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getSocialCallback( $account )
    {
        /**
         * Get the social user
         */
        $socialUser = Socialite::with( $account )->stateless()->user();

        /**
         * Fetch the social user for the user table ??
         */
        $user = User::where('provider_id', '=', $socialUser->id)
            ->where('provider', '=', $account)
            ->first();

        /**
         * Check if the user exists already
         * Create if they do not
         */
        if ($user == null) {
            $newUser = new User();

            $newUser->name          = $socialUser->getName();
            $newUser->email         = $socialUser->getEmail() == '' ? '' : $socialUser->getEmail();
            $newUser->username      = $newUser->email;
            $newUser->avatar        = $socialUser->getAvatar();
            $newUser->password      = '';
            $newUser->provider      = $account;
            $newUser->provider_id   = $socialUser->getId();
            $newUser->role          = 'member';
            $newUser->active        = 1;
            $newUser->activation    = '';
            $newUser->UserId        = '';
            $newUser->UserSecret    = '';
            $newUser->note          = 'Created via Socialite application';
            $newUser->save();

            $user = $newUser;

            // set the account member cookie
            $llct     = time()+60*60*24*180;
            Cookie::queue('app-member', $user->id, $llct, false, false, false, false);

        }

        /**
         * Log in the user
         */
        Auth::login( $user );

        // get a token
        $token =  $user->createToken('AdultMuse')->accessToken;

        return redirect('/#/latest');
    }
}
