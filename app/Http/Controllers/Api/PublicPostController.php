<?php
/**
 * Defines a namespace for the controller.
 */
namespace App\Http\Controllers\Api;

/**
 * Controller
 */
use App\Http\Controllers\Controller;

/**
 * Requests
 */
use Illuminate\Http\Request;
use App\Http\Requests\StoreAdPost;

/**
 * Libraries
 */
use ipinfo\ipinfo\IPinfo;
use ipinfo\ipinfo\IPinfoException;

/**
 * Facades
 */
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Models
 */
use App\Models\Account;
use App\Models\Ad;
use App\Models\Post;
use App\Models\User;

/**
 * Services
 */
use App\Mail\SendPublicLinks;
use App\Notifications\ClickSendSms;


/**
 * Class PublicPostController
 * @package App\Http\Controllers\Api
 */
class PublicPostController extends Controller
{
    /**
     * trigger to remove uploadedImages cookie
     *
     * @var boolean
     */
    private $success = false;
    /**
     * Used in the stringReplace function
     *
     * @var array Used in the stringReplace function
     */
    private $out = array(",","'");

    /**
     * Used in the stringReplace function
     *
     * @var array Used in the stringReplace function
     */
    private $in  = array("&#44;","&#39;");

    /**
     * States array for Australia
     *
     * @var array $states
     */
    private $states = array(
        "New South Wales" => "NSW",
        "Queensland" => "QLD",
        "Northern Territory" => "NT",
        "Western Australia" => "WA",
        "South Australia" => "SA",
        "Victoria" => "VIC",
        "Australian Capitol Territory" => "ACT",
        "Tasmania" => "TAS"
    );

    /**
     * @var \ipinfo\ipinfo\Details|null
     */
    private $ipInfo = null;

    /**
     * @param $out
     * @param $in
     * @param $var
     *
     * @return mixed
     */
    private function stringReplace($out, $in, $var)
    {
        $var = str_replace('"', '', $var);
        return str_replace($out, $in, $var);
    }

    /**
     * Get IPINFO data
     *
     * @param Request $request
     *
     * @return bool|\ipinfo\ipinfo\Details|mixed
     */
    private function getIPInfo(Request $request)
    {
        try {
            $ipInfo = $request->session()->get('ipInfo'); /// Session::get('ipInfo');
            if ($ipInfo) {
                return $ipInfo;
            }
            $ip = config('app.ipinfo_address');
            $access_token = 'e5f368ed86097c';
            $client       = new IPinfo($access_token);
            $ipInfo       = $client->getDetails($ip);
            if ($ipInfo) {
                //// Session::put('ipInfo', $ipInfo);
                $request->session()->put('ipInfo', $ipInfo);
                $request->session()->save();
                $country  = $ipInfo->country_name;
                $llct     = time()+60*60*24*180;
                Cookie::queue('my-country', $country, $llct, false, false, false, false);
                return $ipInfo;
            }
            return false;

        } catch(IPinfoException $e) {
            throw new $e;
        }
    }

    /**
     * Handle the ad posting
     *
     * @param StoreAdPost $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function index(StoreAdPost $request) {
        // get the IP info for user validation and logging
        $this->ipInfo = $request->session()->get('ipInfo');

        if ($this->ipInfo == null) {
            $this->ipInfo = $this->getIPInfo($request);
        }

        // dd($this->ipInfo);

        // capture the post
        $post = $request->validated();

        // preset the uid for public posts
        $userId = 500; /** temporarily set to the default in-house user ID */

       //  dd($post);

        /**
         * Public posts are limited to 2 per phone | email aggregated
         * These are tracked in the public_user DB table
         * If no 'user_key' id provided assume that this is the first post - but we will check anyway
         */
        if (isset($post['userPublicKey'])) {
            $result = Post::where('email', '=', $post['publicEmail'] )
                ->where('phone', '=', $post['publicPhone'])
                ->orWhere('user_key', '=', $post['userPublicKey'])
                ->get();

        } else {
            $result = Post::where('email', '=', $post['publicEmail'] )
                ->where('phone', '=', $post['publicPhone'])
                ->get();
        }

        // dd(count($result));

        /**
         * If this user has public ads then we need to check for membership
         */
        if (count($result) >= 1) {
            // get the users token and reset is a cookie
            $userPublicKey = $result[0]['user_key'];

            $user = User::where('userPublicKey', '=', $userPublicKey)
                ->first();

            if ($user && isset($user['id'])) {
                $userId = $user['id'];

                // check the number of ads this user has
                $ads = Ad::where('user_id', '=', $userId)
                    ->get();

                if (count($ads) >= 5) {
                    // check if user has an account - only members with active account can have more than 5 active ads
                    $accountId = isset($user['account_id']) && $user['account_id'] > 0 ? $user['account_id'] : null;

                    if ($accountId) {
                        // check that the account is active
                        $account = Account::where('account_id', '=', $accountId)
                            ->first();

                        if (!$account || $account['account_status'] == 0) {
                            // inactive account - please reactivate to place more ads
                            return response()->json( array('error' => array( array('type' => 'limit'), array('message' => 'member account inactive'))));

                        } else {
                            $accountType = $account['account_type'];
                            // active premium accounts can have an unlimited number of ads
                            if ($accountType != 'premium') {
                                // limit of 10 ads for basic account
                                if (count($ads) >= 10) {
                                    return response()->json( array('error' => array( array('type' => 'limit'), array('message' => 'account ad limit reached'))));
                                }
                            }
                        }

                    } else {
                        // no account found - basic members are limited to 5 ads
                        return response()->json( array('error' => array( array('type' => 'limit'), array('message' => 'member ad limit reached'))));
                    }
                }

            } else {
                // apply the public guest ad limit
                if (count($result) >= 2) {
                    // handle the excess public ad's error
                    return response()->json( array('error' => array( array('type' => 'limit'), array('message' => 'guest ad limit reached'))));
                    // die("not further posts are allowed for the user with these email|phone combination");
                }
            }

        } else {
            // NO public ads - generate the new user_key
            $userPublicKey = isset($post['userPublicKey']) && strlen($post['userPublicKey']) == 32 ? $post['userPublicKey'] : $this->generateToken( 'user_key',32 );
        }

        // generate the post token
        $postPublicKey = $this->generateToken( 'post_key', 32 );

        $title 		 = isset($post['titleBox']) 	   ? $this->stringReplace($this->out, $this->in, $post['titleBox']) : '';
        $section 	 = isset($post['sectionBox']) 	   ? $post['sectionBox'] 	                                         : false;
        $category    = isset($post['categoryBox'])     ? $post['categoryBox']                                           : false;
        $subCategory = !empty($post['subCategoryBox']) ? $post['subCategoryBox']                                        : false;
        $intro 		 = isset($post['introBox']) 	   ? $this->stringReplace($this->out, $this->in, $post['introBox']) : '';
        $text 		 = isset($post['textBox']) 	       ? $this->stringReplace($this->out, $this->in, $post['textBox'])  : '';
        $age         = isset($post['ageBox'])          ? $post['ageBox']                                                : 18;
        $phone 		 = isset($post['phoneBox']) 	   ? $post['phoneBox']                                              : false;
        $country     = isset($post['countryBox']) 	   ? $post['countryBox']                                            : 'AU';
        $state 	     = isset($post['stateBox'])        ? $post['stateBox']                                              : false;
        $suburb 	 = isset($post['suburbBox'])       ? $post['suburbBox']                                             : false;
        $postcode    = isset($post['postcodeBox'])     ? $post['postcodeBox']                                           : false;
        $postcodes 	 = isset($post['postcodesBox'])    ? $post['postcodesBox']                                          : false;
        $images 	 = isset($post['imagesBox'])       ? $post['imagesBox']                                             : false;
        $video       = isset($post['videoBox'])        ? $post['videoBox']                                              : false;
        $language    = isset($post['languageBox'])     ? $post['languageBox']                                           : 'en';
        $published 	 = isset($post['publishedBox'])    ? $post['publishedBox']                                          : 0;
        $website     = isset($post['websiteBox'])      ? $post['websiteBox']                                            : '';

        $slug_ad     = $this->generateSlug($title);

        if ($website == 'http://' || $website == '0' || $website == 0) { $website = ''; }

        // dd($published);

        if ($published == 2) {
            // get the publishdate value
            $publishDate = isset($post['publishDateBox']) ? $post['publishDateBox'] : false;
            $publishDate = substr($publishDate, 0, strpos($publishDate, 'GMT')-1);
            if ($publishDate != false) {  $publishDate = strtotime($publishDate); }
        }
        else
            $publishDate = NULL;

        if ($language == '' || $language == false) { $language = 'en'; }

        // dd($publishDate);

        // these are all required !!
        if (strlen($title) AND strlen($intro) AND strlen($text) AND strlen($suburb) AND strlen($postcode) AND strlen($postcodes)) {

            // created at reference
            $now = date("Y-m-d h:i:s", time());

            $insertID = DB::table('ad_listings')->insertGetId([
                'title' => $title, 'section_id' => $section, 'category_id' => $category, 'category_sub_id' => $subCategory, 'intro' => $intro,
                'text' => $text, 'phone' => $phone, 'age' => $age, 'country' => $country, 'user_id' => $userId, 'state' => $state, 'suburb' => $suburb,
                'postcode' => $postcode, 'postcodes' => $postcodes, 'images' => $images, 'video' => $video, 'language' => $language, 'published' => $published,
                'website' => $website, 'slug_ad' => $slug_ad, 'post_public_key' => $postPublicKey, 'publish_date' => $publishDate, 'ad_created_at' => $now]);

            if ($insertID >= 1) {
                // handle the images transfer from the sandbox to photos
                $images = explode(",", $images);
                if (is_array($images)) {
                    foreach ($images as $image) {
                        $contents = Storage::disk('sandbox')->get(
                            '/images/' . $image
                        );
                        Storage::disk('photos')->put(
                            '/' . $image,
                            $contents
                        );
                        Storage::disk('sandbox')->delete(
                            '/images/' . $image
                        );
                    }
                }

                // process the images
                $this->doImageInsert( $userId, $userPublicKey, $images);

                // send the links email
                $this->emailLinks($post['publicEmail'], $userPublicKey, $postPublicKey);

                // save the public post record
                $publicPost = new Post();
                $publicPost->user_key = $userPublicKey;
                $publicPost->post_key = $postPublicKey;
                $publicPost->email    = $post['publicEmail'];
                $publicPost->phone    = $post['publicPhone'];
                $publicPost->ipinfo   = \GuzzleHttp\json_encode($this->ipInfo);
                $publicPost->save();

                // ToDo: !!! enable or disable SMS posting here - to save our test credits !!!
                // $this->sendSMS($publicPost, $insertID, config('app.url'));

                // if the user key was not sent from the front end
                if (empty($post['userPublicKey'])) {
                    $pkct     = time()+60*60*24*180;
                    Cookie::queue('AMPK', $userPublicKey, $pkct, false, false, false, false);
                }
                $llct = (1);
                // as this is a Public POST we just send back the ID in the confirmation
                return response()->json( array('record' => array( array('success' => '1'), array('id' => $insertID))))->withCookie(cookie('uploadedImages', false, $llct, false, false, false, false));
                // $llct = (1);
                //return response()->json(array('record' => array( array('success' => '1'), array('id' => $insertID))))->withCookie(cookie('uploadedImages', false, $llct, null, null, false, false));
            }
            return response()->json( array('error' => array( array('type' => 'database'), array('message' => 'insert id not found'))));
        }
        return response()->json( array('error' => array( array('type' => 'data'), array('message' => 'missing'))));

    }

    /**
     * Insert the image upload logs for image tracking and management usage
     *
     * @param $userId
     * @param $userPublicKey
     * @param $images
     */
    private function doImageInsert($userId, $userPublicKey, $images)
    {
        $type = 'image';
        $date = date("Y-m -d h:i:s", time());

        foreach ($images as $image) {
            $size = Storage::disk('photos')->size('/' . $image);
            DB::insert('insert into uploadLogs (file_user_id, user_key, file_type, file_mime, file_name, file_encrypted, file_size, file_created) values (?, ?, ?, ?, ?, ?, ?)',
                [ $userId, $userPublicKey, 'image', $type, $image, $image, $size, $date ]
            );
        }
    }

    /**
     * @param $title
     *
     * @return mixed|string
     */
    private function generateSlug($title = null) : string {
        if ($title) {
            $title = str_replace(array('!','.','-','&#39;','&#44;','&#34;','*','~','(',')','[',']','&','$','^','#'),"", $title);
            $title = str_replace(" ", "-", $title);
            $title = str_replace(" ", "", $title);
            $title = strtolower($title);
        }
        return $title;
    }

    /**
     * Generates a token|key - suitable for password resets etc etc
     *
     * @param string $context The column that will be used to ensure uniqueness of token
     * @param int    $length The length of the token required - default is 24 chars
     *
     * @return string
     */
    public function generateToken( $context, $length = 24) : string {
        // generate the key|token
        $key = $this->randomString($length, true);

        // check that its unique
        $result = Post::where($context, '=', $key)
            ->get();

        if (count($result) == 0) {
            // gtg
            return $key;

        } else {
            // rerun
            return $this->generateToken($context, $length);
        }
    }

    /**
     * Generates a random string
     *
     * @param int  $length
     * @param bool $alphaOnly
     *
     * @return string
     */
    public final function randomString($length = null, $alphaOnly = true) : string {
        if (null === $length || empty($length)) {
            $bits = array(
                24,
                32,
                64,
                128,
                256
            );
            shuffle($bits);
            $length = array_shift($bits);

        } elseif (is_array($length)) {
            shuffle($length);
            $length = array_shift($length);
        }

        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXQZ0123456789';
        if (!$alphaOnly) {
            $chars .= '!#$%^&*().,?~';
        }

        // simplified version returned
        // return $chars[rand(0, strlen($chars)-1];

        mt_srand(10000000 * (double) microtime());

        for ($i = 0, $string = '', $lc = strlen($chars) - 1; $i < $length; $i++) {
            $string .= $chars[mt_rand(0, $lc)];
        }
        return $string;
    }

    /**
     * Handles the emailing process - sends the access key to the ad poster
     *
     * @param $email
     * @param $userPublicKey
     * @param $postPublicKey
     */
    private function emailLinks($email, $userPublicKey, $postPublicKey) {
        Mail::to($email)->send(new SendPublicLinks($userPublicKey, $postPublicKey));
    }

    /**
     * Handles sending the SMS version of the access links
     *
     * @param $publicPost
     * @param $insertId
     * @param $url
     */
    private function sendSMS($publicPost, $insertId, $url) {
        $pp = Post::find($publicPost->id);
        $pp->notify(new ClickSendSms([$insertId, $url]));
    }
}
