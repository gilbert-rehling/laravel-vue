<?php

namespace App\Http\Controllers\Api;

/**
 * @uses
 */
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use ipinfo\ipinfo\IPinfo;
use ipinfo\ipinfo\IPinfoException;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Models\Section;
use App\Models\Category;

/**
 * Class PublicController
 * Handles all current public GET requests
 *
 * @package App\Http\Controllers\Api
 */
class PublicController extends Controller
{
    /**
     * @var null|string $slug
     */
    private $slug = null;

    /**
     * @var int
     */
    private $limit = 30;

    /**
     * States array for Australia
     *
     * @var array $states
     */
    private $states = array(
        "New South Wales" => "NSW",
        "Queensland" => "QLD",
        "Northern Territory" => "NT",
        "Western Australia" => "WA",
        "South Australia" => "SA",
        "Victoria" => "VIC",
        "Australian Capitol Territory" => "ACT",
        "Tasmania" => "TAS"
    );

    /**
     * @var \ipinfo\ipinfo\Details|null
     */
    private $ipInfo = null;

    /**
     * @var string|null
     */
    private $botStatus = null;

    /**
     * PublicController constructor.
     *
     * @param Request $request
     */
//    public function __construct(Request $request)
//    {
//        $this->middleware('age'); // disabled for testing
//    }

    /**
     * Test the User Agent for origin
     *
     * @param Request $request
     * @return string
     */
    private function setBotStatus(Request $request)
    {
        $userAgent = $request->header('User-Agent');
        $r = preg_match('/bot|spider/', $userAgent);
        if ($r) {
            return '';
        }
        return 'u-display-none is-hidden';
    }

    /**
     * @param Request $request
     * @return bool|\ipinfo\ipinfo\Details|mixed
     */
    private function getIPInfo(Request $request)
    {
        try {
            $ipInfo = Session::get('ipInfo');
            if ($ipInfo) {
                return $ipInfo;
            }
            $ip = config('ipinfo_address');
            $access_token = 'e5f368ed86097c';
            $client       = new IPinfo($access_token);
            $ipInfo       = $client->getDetails($ip);
            if ($ipInfo) {
                Session::put('ipInfo', $ipInfo);
                $country  = $ipInfo->country_name;
                $llct     = time()+60*60*24*180;
                Cookie::queue('my-country', $country, $llct, false, false, false, false);
                return $ipInfo;
            }
            return false;

        } catch(IPinfoException $e) {
            throw new $e;
        }
    }

    /**
     * @param $out
     * @param $in
     * @param $var
     *
     * @return mixed
     */
    private function stringReplace($out, $in, $var)
    {
        $var = str_replace('"', '', $var);
        return str_replace($out, $in, $var);
    }

    /**
     * Display a listing of all latest and premium ads.
     *
     * URL:         /api/v1/latest
     * Method:      GET
     * Description: Fetches all ads including premium in latest to oldest order
     *
     * @param Request $request
     * @param null $id
     * @param null $slug
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAds(Request $request, $id = null, $slug = null)
    {
        $this->slug = (isset($slug) ? $slug : (isset($id) ? $id : false));
        try {
            $this->ipInfo = $this->getIPInfo($request);

        } catch(IPinfoException $e) {
            // silent failure
            //echo '<pre>'; var_dump($e); echo '</pre>'; die;
        }

        // get the latest ads
        $latest  = $this->getLatest( $request );

        // always load the premium ads
        $premium = $this->getPremiumAds( $request );

        // get the User Agent status
        $isaBot = $this->setBotStatus( $request );

        return response()->json( array('latest' => $latest, 'premium' => $premium, 'isaBot' => $isaBot));
    }

    /**
     * Fetch all latest ads.
     * Ordered latest to oldest
     *
     * @param Request $request
     *
     * @return array
     */
    public function getLatest(Request $request)
    {
        $language = $request->input('language', 'en');
        $country  = $request->input('country', 'AU');
        $state    = $request->input('state', $this->ipInfo->region);
        $suburb   = $request->input('suburb', $this->ipInfo->city);
        $postcode = $request->input('postcode', $this->ipInfo->postal);

        if ($country == 'AU') {
            // set the state for Db compatibility
            $state = $this->states[$state];
        }

        if ($country && $suburb && $postcode && $state) {
            $this->limit = 100;
            $query = "SELECT * FROM ad_listings al
                    LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                    LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                    LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                    WHERE al.published = 1 AND al.country = ? AND al.state = ? AND al.language = ?
                    ORDER BY (al.suburb = ? OR al.postcode = ?) DESC, (al.ordering_value * al.ordering_multiplier) DESC, al.ad_id DESC
                    LIMIT $this->limit";

            $results = DB::select( $query, [ $country, $state, $language, $suburb, $postcode ] );

        } else {
            $this->limit = 250;
            $query = "SELECT * FROM ad_listings al
                    LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                    LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                    LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                    WHERE al.published = 1 AND al.country = ? AND al.state = ? AND al.language = ?
                    ORDER BY (al.ordering_value * al.ordering_multiplier) DESC
                    LIMIT $this->limit";
            $results = DB::select( $query, [ $country, $state, $language ] );
        }

        $ids = array();
        if ($results) {
            $introOut = array("&#44;","&#39;","<br>","<br />","\"");
            $introIn  = array(",","''","","","");
            $out      = array("&#44;","&#39;","\"","<div>","</div>");
            $in       = array(",","'","","","");
            $output   = array();
            foreach ($results as $row) {
                $ids[]      = $row->ad_id;
                $created    = date("d-m-Y h:i:s", strtotime($row->ad_created_at));
                $date       = date("d-m-Y", strtotime($row->ad_created_at));
                $images     = explode(",", $row->images);
                $imgArray = [];
                foreach ($images as $index => $image) {
                    $imgArray[] = $image; // array("id" => $index, "image" => $image);
                }
                $arr = array(
                    'id'              => $row->ad_id,
                    'title'           => $this->stringReplace($out, $in, $row->title),
                    'intro'           => $this->stringReplace($introOut, $introIn, $row->intro),
                    'text'            => $this->stringReplace($out, $in, $row->text),
                    'images'          => $imgArray,
                    'language'        => $row->language,
                    'age'             => $row->age,
                    'phone'           => $row->phone,
                    'postcodes'       => $row->postcodes,
                    'suburb'          => $row->suburb,
                    'state'           => $row->state,
                    'userId'          => $row->user_id,
                    'rating'          => ((isset($row->advertiser_rating)) ? $row->advertiser_rating : 50),
                    'image_rating'    => ((isset($row->image_rating)) ? $row->image_rating : 0),
                    'views_list'      => $row->total_views_list,
                    'views_ad'        => $row->total_views_ad,
                    'slug_ad'         => $row->slug_ad,
                    'slug_section'    => $row->slug_section,
                    'slug_category'   => $row->slug_category,
                    'published'       => $row->published,
                    'ordering'        => ($row->ordering_multiplier * $row->ordering_value),
                    'created'         => $created,
                    'date'            => $date,
                    'updated'         => $row->ad_updated_at
                );
                $output[] = $arr;
            }
            $sql  = "UPDATE ad_listings SET total_views_list = total_views_list+1 WHERE ad_id IN (" . implode(",",$ids) . ")";
            DB::statement( $sql );

            //echo '</pre>'; var_dump($output); echo '</pre>'; die;
            //dd($output);

            return $output;
        }
    }

    /**
     * Fetch all premium ads
     *
     * @param Request $request
     *
     * @return array
     */
    private function getPremiumAds(Request $request) {
        $language = $request->input('language', 'en');
        $country  = $request->input('country', 'AU');
        $state    = $request->input('state', $this->ipInfo->region);
        $suburb   = $request->input('suburb', $this->ipInfo->city);

        if ($country == 'AU') {
            // set the state for Db compatibility
            $state = $this->states[$state];
        }
        $postcode = $request->input('postcode', $this->ipInfo->postal);

        if ($country && $suburb && $postcode) {
            $query = "SELECT * FROM ad_listings al
                LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                WHERE al.published = 1 AND al.premium = 1 AND al.country = ? AND al.language = ?
                ORDER BY (al.suburb = ? OR al.postcode = ?) DESC, (al.ordering_value * al.ordering_multiplier) DESC
                LIMIT 50";
            $results = DB::select( $query, [ $country, $language, $suburb, $postcode ] );

        } else {
            $query = "SELECT * FROM ad_listings al
                LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
                LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
                LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
                WHERE al.published = 1 AND al.premium = 1 AND al.country = ? AND al.language = ?
                ORDER BY (al.ordering_value * al.ordering_multiplier) DESC
                LIMIT 50";
            $results = DB::select( $query, [ $country, $language ] );
        }
        $ids    = array();
        $output = array();
        if ($results) {
            $introOut = array("&#44;","&#39;","<br>","<br />","\"");
            $introIn  = array(",","''","","","");
            $out      = array("&#44;","&#39;");
            $in       = array(",","'");
            foreach ($results as $row) {
                $ids[]                = $row->ad_id;
                $images = explode(",", $row->images);
                $img    = $images[0];
                $web    = strlen($row->website) ? $row->website : 'javascript:void(0)';
                $arr = array(
                    'id'              => $row->ad_id,
                    'title'           => $this->stringReplace($out, $in, $row->title),
                    'intro'           => $this->stringReplace($introOut, $introIn, $row->intro),
                    'text'            => $this->stringReplace($out, $in, $row->text),
                    'images'          => $img,
                    'language'        => $row->language,
                    'age'             => $row->age,
                    'phone'           => $row->phone,
                    'postcodes'       => $row->postcodes,
                    'suburb'          => $row->suburb,
                    'state'           => $row->state,
                    'userId'          => $row->user_id,
                    'rating'          => ((isset($row->advertiser_rating)) ? $row->advertiser_rating : 50),
                    'image_rating'    => ((isset($row->image_rating)) ? $row->image_rating : 0),
                    'views_list'      => $row->total_views_list,
                    'views_ad'        => $row->total_views_ad,
                    'slug_ad'         => $row->slug_ad,
                    'slug_section'    => $row->slug_section,
                    'slug_category'   => $row->slug_category,
                    'published'       => $row->published,
                    'website'         => $web,
                    'ordering'        => ($row->ordering_multiplier * $row->ordering_value),
                    'created'         => $row->ad_created_at,
                    'updated'         => $row->ad_updated_at
                );
                $output[] = $arr;
            }
            $sql  = "UPDATE ad_listings SET total_views_list = total_views_list+1 WHERE ad_id IN (" . implode(",",$ids) . ")";
            DB::statement( $sql );
        }
        return $output;
    }

    /**
     * Display the specified ad.
     *
     * URL:         /api/v1/latest/{slug}
     * Method:      GET
     * Description: Fetches a single ad using the provided slug
     *
     * @param    Request $request
     * @param    int  $slug
     *
     * @return   \Illuminate\Http\Response
     */
    public function getAd(Request $request, $slug)
    {
        // get a single ad
        $query = "SELECT * FROM ad_listings al
            LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
            LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
            LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
            WHERE al.published = 1 AND (al.ad_id = ? OR al.slug_ad = ?)";

        $results = DB::select( $query, [$slug, $slug] );
        if ($results) {
            $introOut = array("&#44;","&#39;","<br>","<br />","\"");
            $introIn  = array(",","''","","","");
            $out      = array("&#44;","&#39;","\r");
            $in       = array(",","'","");

            foreach ($results as $row) {
                $video = (($row->video != 0 && $row->video) != '' ? $row->video : '');
                $arr = array(
                    'id'              => $row->ad_id,
                    'title'           => $this->stringReplace($out, $in, $row->title),
                    'intro'           => $this->stringReplace($introOut, $introIn, $row->intro),
                    'text'            => $this->stringReplace($out, $in, $row->text),
                    'images'          => trim($row->images,","),
                    'video'           => $video,
                    'language'        => $row->language,
                    'age'             => $row->age,
                    'phone'           => $row->phone,
                    'postcodes'       => $row->postcodes,
                    'suburb'          => $row->suburb,
                    'state'           => $row->state,
                    'country'         => $row->country,
                    'userId'          => $row->user_id,
                    'rating'          => ((isset($row->advertiser_rating)) ? $row->advertiser_rating : 50),
                    'image_rating'    => ((isset($row->image_rating)) ? $row->image_rating : 0),
                    'comments'        => '"'.$row->comments.'"',
                    'views_list'      => $row->total_views_list,
                    'views_ad'        => $row->total_views_ad,
                    'slug_ad'         => $row->slug_ad,
                    'slug_section'    => $row->slug_section,
                    'slug_category'   => $row->slug_category,
                    'published'       => $row->published,
                    'website'         => $row->website,
                    'created'         => $row->ad_created_at,
                    'updated'         => $row->ad_updated_at
                );
            }
            $query  = "UPDATE ad_listings SET total_views_ad = total_views_ad+1 WHERE ad_id = ?";
            $result = DB::update( $query, [$slug] );
        }

        return response()->json( array('ad' => $arr, 'isABot' => $this->setBotStatus( $request )) );
    }

    /**
     * Display the specified ad.
     *
     * URL:         /api/v1/latest/ad/{id}
     * Method:      GET
     * Description: Fetches a single ad using the provided ad_id - regardless of published state
     *
     * @param    Request $request
     * @param    int  $id
     *
     * @return   \Illuminate\Http\Response
     */
    public function getNewAd(Request $request, $id)
    {
        // get a single ad
        $query = "SELECT * FROM ad_listings al
            LEFT JOIN sectionListings sl ON (al.section_id = sl.section_id)
            LEFT JOIN categoryListings cl ON (al.category_id = cl.category_id)
            LEFT JOIN advertiserReferences ar ON (al.ad_id = ar.advertiser_ad_id)
            WHERE al.ad_id = ?";

        $results = DB::select( $query, [$id] );
        if ($results) {
            $introOut = array("&#44;","&#39;","<br>","<br />","\"");
            $introIn  = array(",","''","","","");
            $out      = array("&#44;","&#39;","\r");
            $in       = array(",","'","");

            foreach ($results as $row) {
                $video = (($row->video != 0 && $row->video) != '' ? $row->video : '');
                $arr = array(
                    'id'              => $row->ad_id,
                    'title'           => $this->stringReplace($out, $in, $row->title),
                    'intro'           => $this->stringReplace($introOut, $introIn, $row->intro),
                    'text'            => $this->stringReplace($out, $in, $row->text),
                    'images'          => trim($row->images,","),
                    'video'           => $video,
                    'language'        => $row->language,
                    'age'             => $row->age,
                    'phone'           => $row->phone,
                    'postcodes'       => $row->postcodes,
                    'suburb'          => $row->suburb,
                    'state'           => $row->state,
                    'country'         => $row->country,
                    'userId'          => $row->user_id,
                    'rating'          => ((isset($row->advertiser_rating)) ? $row->advertiser_rating : 50),
                    'image_rating'    => ((isset($row->image_rating)) ? $row->image_rating : 0),
                    'comments'        => '"'.$row->comments.'"',
                    'views_list'      => $row->total_views_list,
                    'views_ad'        => $row->total_views_ad,
                    'slug_ad'         => $row->slug_ad,
                    'slug_section'    => $row->slug_section,
                    'slug_category'   => $row->slug_category,
                    'published'       => $row->published,
                    'website'         => $row->website,
                    'created'         => $row->ad_created_at,
                    'updated'         => $row->ad_updated_at
                );
            }
            $query  = "UPDATE ad_listings SET total_views_ad = total_views_ad+1 WHERE ad_id = ?";
            $result = DB::update( $query, [$id] );
        }

        return response()->json( array('ad' => $arr, 'isABot' => $this->setBotStatus( $request )) );
    }

    /**
     * Returns all ad section types
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSections(Request $request)
    {
        $sections = Section::all();

        return response()->json( $sections );
    }

    /**
     * Return all add primary categories
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories(Request $request)
    {
        $categories= Category::all();

        return response()->json( $categories );
    }

    /**
     * Return all ad sub-categories matching the provided parent id
     *
     * @param Request $request
     * @param $parent
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubCategories(Request $request, $parent)
    {
        $subCategories= Category::where('category_parent', '=', $parent )->get();

        return response()->json( array('subCategories' => $subCategories) );
    }
}
