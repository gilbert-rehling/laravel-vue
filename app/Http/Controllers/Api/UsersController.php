<?php
/*
  Defines a namespace for the controller.
*/
namespace App\Http\Controllers\Api;

/*
  Uses the controller interface.
*/
use App\Http\Controllers\Auth\RegisterController;

/*
  Defines the requests used by the controller.
*/
use Illuminate\Http\Request;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\RegisterUserRequest;

/*
  Defines the models used by the controller.
*/
use App\Models\User;
use App\Models\State;
use App\Models\Postcode;

/*
  Defines the facades etc used by the controller.
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

/*
  Defined the libraries used
 */
use Illuminate\Validation\ValidationException;
use ipinfo\ipinfo\IPinfo;
use ipinfo\ipinfo\IPinfoException;
use Mockery\Exception;

/**
 * Class UsersController
 * The user controller handles all API requests that need to manage user data.
 * Todo: !! before staging find and add the email validation code I used before !!
 *
 * @package App\Http\Controllers\Api
 */
class UsersController extends RegisterController
{
    /**
     * @param Request $request
     * @return bool|\ipinfo\ipinfo\Details|mixed
     */
    private function getIPInfo(Request $request)
    {
        try {
            $ipInfo = Session::get('ipInfo');
            if ($ipInfo) {
                return $ipInfo;
            }
            $ip = config('ipinfo_address');
            $access_token = 'e5f368ed86097c';
            $client       = new IPinfo($access_token);
            $ipInfo       = $client->getDetails($ip);
            if ($ipInfo) {
                Session::put('ipInfo', $ipInfo);
                $country  = $ipInfo->country_name;
                $llct     = time()+60*60*24*180;
                Cookie::queue('my-country', $country, $llct, false, false, false, false);
                return $ipInfo;
            }
            return false;

        } catch(IPinfoException $e) {
            throw new $e;
        }
    }

    /*
    |------------------------------------------------- ------------------------------
    | Get User
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Method:         GET
    | Description:    Gets the authenticated user
    */
    public function getUser(Request $request)
    {
        $user = auth('api')->user();
        return response()->json($user->getAttributes());
    }

    /*
    |-------------------------------------------------------------------------------
    | Get Users
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/users
    | Method:         GET
    | Description:    Gets the users searched by the authenticated user.
    */
    public function getUsers(Request $request)
    {
        $query = $request->get('search');

        $users = User::where('name', 'LIKE', '%'.$query.'%')
         ->orWhere('email', 'LIKE', '%'.$query.'%')
         ->get();

        return response()->json( $users );
    }

    /*
    |-------------------------------------------------------------------------------
    | Registers a User's Profile
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Method:         POST
    | Description:    Updates the authenticated user's profile
    */
    public function postRegisterUser( Request $request )
    {
        try {
            // validate
            $result = $this->validate( $request, array(
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required'
            ));

        //    dd($result);

            // create user
            $user = $this->create( $result );
           //event(new Registered($user = $this->create($request->all())));

            $token =  $user->createToken('AdultMuse')->accessToken;

            // auto login
            $this->guard()->login($user);

            // set the account member cookie
            $llct     = time()+60*60*24*180;
            Cookie::queue('app-member', $user->id, $llct, false, false, false, false);

            /*
            *   Return a response that the user was updated successfully.
            */
            return response()->json( ['user_created' => true, 'token' => $token], 201 );

        }
        catch (ValidationException $e) {
            dd($e);
            throw new $e;

        }
        catch( Exception $e) {
            dd($e);
        }
    }

    /*
    |-------------------------------------------------------------------------------
    | Updates a User's Profile
    |-------------------------------------------------------------------------------
    | URL:            /api/v1/user
    | Method:         PUT
    | Description:    Updates the authenticated user's profile
    */
    public function putUpdateUser( EditUserRequest $request )
    {
        $user = Auth::user();

        $user->save();

        /*
        *   Return a response that the user was updated successfully.
        */
        return response()->json( ['user_updated' => true], 201 );
    }

    /**
     * Checks if an email address exists already
     *
     * @param Request $request
     * @param $email
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmail(Request $request, $email)
    {
        if ($email) {
        //    $query = $request->get('search');

            $users = User::where('username', 'LIKE', '%'.$email.'%')
                ->orWhere('email', 'LIKE', '%'.$email.'%')
                ->get();

            if (count($users) > 0) {
                $result = array("success" => true, "record" => array("email" => array("notFound" => 0, "valid" => $this->validEmail($email))));

            } else {
                $result = array("success" => true, "record" => array("email" => array("notFound" => 1, "valid" => $this->validEmail($email))));
            }
            return response()->json( $result );
        }
    }

    /**
     * Returns the users location data
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserLocation(Request $request)
    {
        $location = $this->getIPInfo($request);

        return response()->json( $location->all );

    }

    /**
     * Returns the users state
     *
     * @param Request $request
     * @param $country
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserStates(Request $request, $country)
    {
        $states = State::where('country_code', '=', $country)
        ->get();

        if (count($states)) {
            $result = array("success" => true, "states" => $states);

        } else {
            $result = array("success" => true, "states" => array());
        }
        return response()->json( $result );
    }

    /**
     * Returns the users postcode
     *
     * @param Request $request
     * @param $country
     * @param $state
     * @param $suburb
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserPostcode(Request $request, $country, $state, $suburb)
    {
        $postcode = Postcode::where('country_code', '=', $country)
            ->where('state_code', '=', $state)
            ->where('suburb', '=', $suburb)
            ->get();

        if (count($postcode)) {
            $result = array("success" => true, "postcode" => $postcode);

        } else {
            $result = array("success" => true, "postcode" => false);
        }
        return response()->json( $result );
    }

    /**
     * Returns the users postcode
     *
     * @param Request $request
     * @param $country
     * @param $state
     * @param $postcode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserSuburb(Request $request, $country, $state, $postcode)
    {
        $suburb = Postcode::where('country_code', '=', $country)
            ->where('state_code', '=', $state)
            ->where('postcode', '=', $postcode)
            ->get();

        if (count($suburb)) {
            $result = array("success" => true, "suburb" => $suburb);

        } else {
            $result = array("success" => true, "suburb" => false);
        }
        return response()->json( $result );
    }

    /**
     * Returns the users area/suburb
     *
     * @param Request $request
     * @param $country
     * @param $state
     * @param $value
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserArea(Request $request, $country, $state, $value)
    {
        if (is_numeric($value)) {
            // use postcode as reference //  LEFT JOIN states s ON (s.code = p.state_code)
            if ($state) {
                // use state
                $result = Postcode::where('country_code', '=', $country)
                    ->where('state_code', '=', $state)
                    ->where('postcode', 'LIKE', $value .'%')
                    ->orderBy('suburb')
                    ->offset(0)
                    ->limit(25)
                    ->get();

            } else {
                // no state
                $result = Postcode::where('country_code', '=', $country)
                    ->where('postcode', 'LIKE', $value .'%')
                    ->orderBy('suburb')
                    ->offset(0)
                    ->limit(25)
                    ->get();
            }

            if (isset($result)) {
                // result found
                $output = array();
                foreach ($result as $row) {
                    $arr = array(
                        'postcode'  => $row->postcode,
                        'suburb'    => ucwords(strtolower($row->suburb))
                    );
                    $output[] = $arr;
                }

                return response()->json( array('success' => true, 'records' => $output) );

            } else {
                // no previous result
                if ($state) {
                    // try without the state reference
                    $result = Postcode::where('country_code', '=', $country)
                        ->where('postcode', 'LIKE', $value .'%')
                        ->orderBy('suburb')
                        ->offset(0)
                        ->limit(25)
                        ->get();

                    if (isset($result)) {

                        $output = array();
                        foreach ($result as $row) {
                            $arr = array(
                                'postcode'  => $row->postcode,
                                'suburb'    => ucwords(strtolower($row->suburb))
                            );
                            $output[] = $arr;
                        }
                        return response()->json( array('success' => true, 'records' => $output) );

                    } else {
                        return response()->json( array('success' => true, 'records' => false) );
                    }

                } else {
                    return response()->json( array('success' => true, 'records' => false) );
                }
            }

        } else {
            // use suburb as reference
            if ($state) {
                // use state
                $result = Postcode::where('country_code', '=', $country)
                    ->where('state_code', '=', $state)
                    ->where('suburb', 'LIKE', $value .'%')
                    ->orderBy('suburb')
                    ->offset(0)
                    ->limit(25)
                    ->get();

            } else {
                // no state
                $result = Postcode::where('country_code', '=', $country)
                    ->where('suburb', 'LIKE', $value .'%')
                    ->orderBy('suburb')
                    ->offset(0)
                    ->limit(25)
                    ->get();
            }

         ///   dd($result);

            if (isset($result)) {

                $output = array();
                foreach ($result as $row) {
                    $arr = array(
                        'postcode'  => $row->postcode,
                        'suburb'    => ucwords(strtolower($row->suburb))
                    );
                    $output[] = $arr;
                }
                return response()->json( array('success' => true, 'records' => $output) );

            } else {
                if ($state) {
                    // try without the state reference
                    //$query = "SELECT * FROM `postcode` WHERE `suburb` LIKE ? AND `country_code` = ? ORDER BY `suburb` ASC LIMIT 25";
                    //$result = DB::select( $query, [$value.'%', $cid] );
                    $result = Postcode::where('country_code', '=', $country)
                        ->where('suburb', 'LIKE', $value .'%')
                        ->orderBy('suburb')
                        ->offset(0)
                        ->limit(25)
                        ->get();

                    if (isset($result)) {

                        $output = array();
                        foreach ($result as $row) {
                            $arr = array(
                                'postcode'  => $row->postcode,
                                'suburb'    => ucwords(strtolower($row->suburb))
                            );
                            $output[] = $arr;

                        }
                        return response()->json( array('success' => true, 'records' => $output) );

                    } else {
                        return response()->json( array('success' => true, 'records' => false) );
                    }

                } else {
                    return response()->json( array('success' => true, 'records' => false) );
                }
            }
        }
    }

    /**
     * Placeholder for email validation method - once the script is found buried in my archive of code
     *
     * @param $email
     * @return bool
     */
    private function validEmail( $email) {
        return 1;
    }
}
