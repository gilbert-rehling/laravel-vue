<?php

namespace App\Http\Controllers\Api;

/**
 * @uses
 */
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
//use App\Models\User;
//use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Contracts\Auth\Guard;
//use Illuminate\Contracts\Filesystem\Filesystem;


//use FFMpeg\FFProbe as Probe;
//use FFMpeg\FFMpeg as FFMpeg;
//use FFMpeg\Coordinate\Dimension as Dimension;
//use FFMpeg\Coordinate\TimeCode as TimeCode;
//use FFMpeg\Format\Video\X264 as X264;
//use FFMpeg\Format\ProgressListener\VideoProgressListener as Listener;
//use FFMpeg\Driver\FFProbeDriver as Driver;
//use Alchemy\BinaryDriver\ProcessBuilderFactoryInterface as Alchemy;

/**
 * Class PicturesController
 * ToDo: !! This controller was intended for members area picture management within Laravel rendered templates !!
 *       !! I may reuse this once the context of a members area for authed users in created - for media management and overview !!
 *
 * @package App\Http\Controllers\Api
 */
class PicturesController extends Controller
{
    /**
    * String array of uploaded Pictures - used to save as a cookie value
    * - in case the user restart the Add page after uploading and before submitting,
    * - we can re-use the image they already uploaded
    *
    * @var string
    */
    public $uploadedPictures = false;

    /**
    * String contains saved uploadedPictures cookie data
    *
    * @var string
    */
    public $savedPictures    = false;

    /**
    * trigger to remove uploadedImages cookie
    *
    * @var boolean
    */
    private $success = false;

    /**
    * Used in the stringReplace function
    *
    * @var array Used in the stringReplace function
    */
    private $out = array(",","'");

    /**
    * Used in the stringReplace function
    *
    * @var array Used in the stringReplace function
    */
    private $in  = array("&#44;","&#39;");

    //$this->disk = Storage::disk(config('blog.uploads.storage'));

    /**
    * Handle Adult Muse picture data management.
    * Methods for handling insert, update, delete
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $function, $id = null) {

        $this->savedPictures = $request->cookie('uploadedPictures');
        DB::enableQueryLog();

        // process picture data
        switch ($function) {

            case 'uploadPicture':
                $output = $this->uploadImage($request, $id);
                break;

            case 'addPicture':
                $output = $this->insertPicture($request, $id);
                break;

            //      case 'updatePicture':
            //        $output = $this->updatePicture($request, $id);
            //        break;
            //
            //      case 'updatePictureAd':
            //        $output = $this->updatePictureAd($request, $id);
            //        break;

            case 'deletePicture':
                $output = $this->deletePicture($request, $id);
                break;

            case 'selectUserPictures':
                $output = $this->selectUserPictures($request, $id);
                break;

            case 'getAllUserPictures':
                $output = $this->getAllUserPictures($request, $id);
                break;

            case 'getUserPictures':
                $output = $this->getUserPictures($request, $id);
                break;

            default:
                $output = [];
                return response()->json($output);

        }

        /* default cookie duration */
        $llct  = (60*24);

        /* return response */
        if ($function == 'addPicture') {
          return response()->json($output)->withCookie(cookie('uploadedPictures', $this->uploadedPictures, $llct, null, null, false, false));

        } else {
          return response()->json($output);
        }

    }

    /**
    * String Replace Method for convenience
    * We can swap the IN's and OUT's when calling this method
    *
    * @param type $out
    * @param type $in
    * @param type $var
    * @return type string
    */
    private function stringReplace($out, $in, $var) {
        $val = stripslashes($var);
        return str_replace($out, $in, $val);
    }

    /**
    * Global function to decode and sanitise all JSON post data
    * The code in Angular front-end is using JSON.stringify() before sending via AJAX
    *
    * @return type array
    */
    private function getPostData() {
        $post   = new Request();
        $req    = $post->json();
        // if the post arrives as an object we need to do some pre-processing
        if (is_object($req) AND count($req) == 1) {
          // this peels the arrayed object out of the JSON - unable to use $req[0]
          foreach ($req as $arr) { $new = $arr; }
          // because we are using stringyfoed JSON we had to build a decoder
          $clr    = array("{","}");
          // the only problem here is 'commas' (,) in the data - we have to use data.replace(",","&#44;") in the front-end
          $data   = explode(",", str_replace($clr, "", trim($new, "\"")));
          $input  = array();
          // finishes converting the JSON object into an array
          foreach ($data as $d) {
              $arr = explode(":", $d);
              if (isset($arr[0], $arr[1])) {
                  $input[trim($arr[0],"\"")] = trim(str_replace("'","&#39;",$arr[1]),"\"");
              }
          }
        } else {
          $input = $req;
        }
        $arr = array();
        // a bit of sanitising for sanity sake
        foreach ($input as $key => $value) {
          $v      = $val = false;
          $val    = trim($value);
          $v      = filter_var($val, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
          $value  = filter_var($v, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
          $arr[$key] = $value;
        }
        return $arr;
    }

    /**
    * Insert a Picture
    */
    private function insertPicture($request, $id) {

        $user = auth()->user();
        $user = $user['attributes'];
        $uid  = $user['id'];

        if ($id == $uid) {

            $POST   = $this->getPostData();
            $type = false;

            $title        = isset($POST['title'])       ? $this->stringReplace($this->out, $this->in, $POST['title'])       : false;
            $description  = isset($POST['description']) ? $this->stringReplace($this->out, $this->in, $POST['description']) : false;
            $section      = isset($POST['section'])     ? $POST['section']                                                  : false;
            $userId       = $uid;
            $adId         = isset($POST['ad_id'])       ? $POST['ad_id']                                                    : false;
            $size         = isset($POST['size'])        ? $POST['size']                                                     : false;
            $type         = isset($POST['type'])        ? $POST['type']                                                     : false;
            $published    = isset($POST['published'])   ? $POST['published']                                                : 0;

            // file ID as set in the uploadLogs table
            $fileId       = isset($POST['fileId'])      ? $POST['fileId']                                                   : false;

            // if 'type' is false and fileId is present - get the Picture type (mime)
            if ($type == false && is_numeric($fileId)) {
                $query    = "SELECT file_mime as mime FROM uploadLogs WHERE file_id = ? and file_user_id = ? LIMIT 01";
                $result   = DB::select( $query, [ $fileId, $uid] );
                if (isset($result[0]->mime)) {
                    $type   = $result[0]->mime;
                }
            }

            // this is a reference to the Pictures 'encrypted' filename
            $picture      = isset($POST['picture'])       ? $POST['picture']                                                    : false;

            // created_at reference
            $now        = date("Y-m-d h:i:s", time());

            if (strlen($title) AND strlen($description) AND strlen($section) AND strlen($userId) AND strlen($fileId)) {

                $insertID = DB::table('pictureData')->insertGetId(
                    ['picture_title' => $title, 'picture_description' => $description, 'picture_section' => $section, 'picture_file_id' => $fileId, 'picture_user_id' => $userId, 'picture_ad_id' => $adId, 'picture_size' => $size, 'picture_type' => $type, 'picture_published' => $published, 'picture_created_at' => $now]);

                if ($insertID >= 1) {
                    $ad_id = false;
                    $title = false;
                    if ($picture != false) {
                        // check if this picture is used in an ad
                        $query  = "SELECT ad_id as ad_id, title as title FROM adListings
                            WHERE picture = ? AND user_id = ? ORDER BY ad_id DESC LIMIT 01";
                        $result = DB::select( $query, [$picture, $uid] );

                        if (isset($result[0]->ad_id)) {
                            $ad_id = $result[0]->ad_id;
                            $title = $result[0]->title;
                            // update new insert
                            DB::table('pictureData')->where('picture_id', $insertID)->where('picture_user_id', $uid)->update(['picture_ad_id' => $ad_id]);
                        }
                    }
                    $this->success = true;
                    return array('record' => array( array('success' => '1'), array('id' => $insertID), array('ad_id' => $ad_id), array('title' => $title)));
                }
                return array('error' => array( array('type' => 'database'), array('message' => 'insert id not found')));
            }
            return array('error' => array( array('type' => 'data'), array('message' => 'missing')));
        }
        return array('error' => array( array('type' => 'uid'), array('message' => 'not found')));
    }

    private function selectUserPictures($request, $id) {

        $user = auth()->user();
        $user = $user['attributes'];
        $uid  = $user['id'];

        if ($id == $uid) {

            $POST = $this->getPostData();
            $file = isset($POST['picture']) ? $POST['picture'] : false;
            $edit = isset($POST['edit'])    ? $POST['edit'] : false;

            if (strlen($file == 0)) { $file = $request->input('picture'); }

            if ($file || $edit == 'edit') {

                if ($edit == 'edit') {
                     $query = "SELECT ul.file_name, ul.file_encrypted, ul.file_size 
                        FROM uploadLogs ul
                        WHERE ul.file_user_id = ? AND ul.file_type = ? AND ul.file_status = 'available'
                        ORDER BY file_id DESC";
                      $results = DB::select( $query, [$uid, 'image'] );

                } else {
                    $query = "SELECT ul.file_name, ul.file_encrypted, ul.file_size 
                        FROM uploadLogs ul
                        WHERE ul.file_encrypted != ? AND ul.file_user_id = ? AND ul.file_type = ? AND ul.file_status = 'available'
                        ORDER BY file_id DESC";
                    $results = DB::select( $query, [$file, $uid, 'image'] );
                }

                if (isset($results) AND count($results)) {
                    $output = array();
                    foreach ($results as $row) {
                        $arr = array(
                            'file'    => $row->file_name,
                            'size'    => $row->file_size,
                            'picture' => $row->file_encrypted
                        );
                        $output[] = $arr;
                    }
                    return array('records' => $output);
                }
                return array('error' => array( array('type' => 'data'), array('message' => 'no data returned')));
            }
            return array('error' => array( array('type' => 'file'), array('message' => 'file name missing for exclusion query value')));
        }
        return array('error' => array( array('type' => 'uid'), array('message' => 'user id not found')));
    }

    /**
    * Get a Picture file type and update the associated DB records
    *
    * @param $file_encrypted string
    * @param $file_id int
    * @param $picture_id int
    *
    * @return string Filesize
    */
    private function updatePictureFileType($file_encrypted, $uid, $file_id) {
        if ($file_encrypted) {
            $path   = storage_path() . '/photos/';
            if (file_exists($path . $file_encrypted)) {
                $type = Storage::mimeType( "/photos/" . $file_encrypted );
                // update file upload log table record
                DB::table('uploadLogs')->where('file_id', $file_id)->where('file_user_id', $uid)->update([ 'file_mime' => $type ]);
                return $type;
            }
        }
        return null;
    }

    /**
    * Get a Picture file size and update the associated DB records
    *
    * @param $file_encrypted string
    * @param $file_id int
    * @param $picture_id int
    *
    * @return string Filesize
    */
    private function updatePictureFileSize($file_encrypted, $uid, $file_id) {
        if ($file_encrypted) {
            $path   = storage_path() . '/photos/';
            if (file_exists($path . $file_encrypted)) {
                $size = Storage::size( "/photos/" . $file_encrypted );
                // update file upload log table record
                DB::table('uploadLogs')->where('file_id', $file_id)->where('file_user_id', $uid)->update([ 'file_size' => $size ]);
                return $size;
            }
        }
        return null;
    }

    /**
    * Return sizes readable by humans
    */
    private function human_filesize($bytes, $decimals = 2) {
        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    /**
    * gets all users pictures to display in the Picture manager
    */
    private function getAllUserPictures($request, $id) {

        $user = auth()->user();
        $user = $user['attributes'];
        $uid  = $user['id'];

        if ($id == $uid) {

            $query = "SELECT 
                ul.file_id, ul.file_name, ul.file_mime, ul.file_encrypted, ul.file_size, ul.file_created, 
                al.ad_id, al.title as ad_title  
                FROM uploadLogs ul
                LEFT JOIN adListings al ON al.images LIKE CONCAT('%', ul.file_encrypted ,'%')
                WHERE ul.file_user_id = ? AND ul.file_type = ? AND ul.file_status != 'deleted'
                ORDER BY ul.file_id ASC";

            $results = DB::select( $query, [$uid, 'image'] );

            if (isset($results) AND count($results)) {
                $output = array();
                $path   = storage_path() . '/photos/';
                foreach ($results as $row) {
                    if ($row->file_size == 0) {
                        $fileSize = $this->updatePictureFileSize($row->file_encrypted, $uid, $row->file_id);
                        if ($fileSize) {
                            $row->file_size = $fileSize;
                        }
                    }
                    if ($row->file_mime == 0 || $row->file_mime == '') {
                        $fileType = $this->updatePictureFileType($row->file_encrypted, $uid, $row->file_id);
                        if ($fileType) {
                            $row->file_mime = $fileType;
                        }
                    }
                    if (file_exists(storage_path() . '/photos/' . $row->file_encrypted)) {
                          $arr = array(
                            'file_id'           => $row->file_id,
                            'file_name'         => $row->file_name,
                            'file_mime'         => $row->file_mime,
                            'file_encrypted'    => $row->file_encrypted,
                            'file_size'         => $this->human_filesize($row->file_size),
                            'file_bytes'        => $row->file_size,
                            'file_created'      => $row->file_created,
                            'ad_id'             => $row->ad_id,
                            'ad_title'          => $row->ad_title
                          );
                        $output[] = $arr;
                    } else {
                        $upd = DB::table('uploadLogs')->where('file_id', $row->file_id)->where('file_user_id', $uid)->update(['file_status' => 'deleted']);
                    }
                }
                return array('records' => $output);
            }
            return array('error' => array( array('type' => 'data'), array('message' => 'no data retured')));
        }
        return array('error' => array( array('type' => 'uid'), array('message' => 'user id not found')));
    }

    /**
    * gets all users pictures to display in the Picture selector
    */
    private function getUserPictures($request, $id) {

        $user = auth()->user();
        $user = $user['attributes'];
        $uid  = $user['id'];

        if ($id == $uid) {

            $query = "SELECT ul.file_encrypted  
                FROM uploadLogs ul
                WHERE ul.file_user_id = ? AND ul.file_type = ? AND ul.file_status != 'deleted' AND ul.file_type = 'image'
                ORDER BY ul.file_id ASC";

            $results = DB::select( $query, [$uid, 'image'] );

            if (isset($results) AND count($results)) {
                $output = array();
                foreach ($results as $row) {
                    if (file_exists(storage_path() . '/photos/' . $row->file_encrypted)) {
                        $arr = array(
                            'file_encrypted'    => $row->file_encrypted
                        );
                        $output[] = $arr;
                    }
                }
                return array('records' => $output);
            }
            return array('error' => array( array('type' => 'data'), array('message' => 'no data retured')));
        }
        return array('error' => array( array('type' => 'uid'), array('message' => 'user id not found')));
    }

    /**
    * Publish or Un-Publish a Picture listing
    *
    * @return type array
    */
    private function publishPicture($request, $id) {
        $user       = auth()->user();
        $attr       = $user['attributes'];
        $uid        = $attr['id'];
        if ($uid == $id) {
            $POST  = $this->getPostData();
            $vid    = $POST['vidID'];
            if (is_numeric($vid)) {
                $job    = $POST['job'];
                if ($job == 'publish') {
                    $result = DB::table('videoData')->where('video_id', $vid)->where('video_user_id', $uid)->update(['video_published' => 1]);
                    if ($result) {
                        return array('record' => array('id' => $vid));
                    }
                    return array('error' => array( array('type' => 'database'), array('message' => 'publish database error')));

                } elseif ($job == 'unpublish') {
                    $result = DB::table('videoData')->where('video_id', $vid)->where('video_user_id', $uid)->update(['video_published' => 0]);
                    if ($result) {
                        return array('record' => array('id' => $vid));
                    }
                    return array('error' => array( array('type' => 'database'), array('message' => 'publish database error')));
                }
                return array('error' => array( array('type' => 'action'), array('message' => 'publish job error')));
            }
            return array('error' => array( array('type' => 'videoid'), array('message' => 'video ID missing')));
        }
        return array('error' => array( array('type' => 'uid'), array('message' => 'not found')));
    }

    /**
    * Delete a picture from PictureData and ddlete the file (leave the upload record intact)
    *
    * @return array
    */
    private function deletePicture($request, $id) {

        $user       = auth()->user();
        $attr       = $user['attributes'];
        $uid        = $attr['id'];
        if ($uid == $id) {
            $POST  = $this->getPostData();
            $del    = $POST['delete'];
            $pid    = $POST['pid'];
            if (is_numeric($pid) AND $id == $del) {
                // get the file name and file ID
                $query = "SELECT file_id, file_encrypted FROM uploadLogs ul
                     WHERE vd.file_id = ? AND vd.file_user_id = ?
                    LIMIT 01";

                $result = DB::select( $query, [$pid, $uid] );

                if (isset($result[0]->file_encrypted) AND isset($result[0]->file_id)) {

                    $fileId = $result[0]->file_id;
                    $file   = $result[0]->file_encrypted;

                    if ($fileId) {

                        $destinationPath  = storage_path() . '/photos/';
                        if (file_exists($destinationPath . $file)) { unlink( $destinationPath . $file); }
                        $jpeg = str_replace("mp4", "jpg", $file);
                        if (file_exists($destinationPath . $jpeg)) { unlink( $destinationPath . $jpeg); }

                        $result = DB::table('uploadLogs')->where('file_id', $fileId)->where('file_user_id', $uid)->update(['file_status' => 'deleted']);

                        return array('record' => array('id' => $fileId));

                    }
                    return array('error' => array( array('type' => 'database'), array('message' => 'picture data not deleted')));
                }
                return array('error' => array( array('type' => 'database'), array('message' => 'file data not selected')));
            }
            return array('error' => array( array('type' => 'pictureid'), array('message' => 'picture ID missing')));
        }
        return array('error' => array( array('type' => 'uid'), array('message' => 'not found')));
    }

    public function uploadImage(Request $request)
    {
        $destination = config('sandbox_images');
    }

}
