<?php

namespace App\Http\Controllers\Api;

/**
 * @uses
 */
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Filesystem\Filesystem;

/* these will be used later on for handling video decoding */
//use FFMpeg\FFMpeg as FFMpeg;
//use FFMpeg\Coordinate\Dimension as Dimension;
//use FFMpeg\Coordinate\TimeCode as TimeCode;
//use FFMpeg\Format\Video\X264 as X264;


/**
 * Class FilesController
 * This controller is for rudimentary file handling requirements
 *
 * @package App\Http\Controllers\Api
 */
class FilesController extends Controller
{
    /**
     * String array of uploaded images - used to save as a cookie value
     * - in case the user restarts the Add page after uploading and before submitting,
     * - we can re-use the image they already uploaded
     */
    public $uploadedImages = false;

    public $savedImages    = false;

    public $uploadedVideos = false;

    public $savedVideos    = false;

    /**
     * Handle AM file uploads.
     * Methods for handling images and videos
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $function, $context = 'public', $id = null)
    {
      // get uploaded image
      $llct        = (60*24);
      if ($function == 'image') {

            $this->savedImages = $request->cookie('uploadedImages');
            $output = $this->uploadImage($request, $context, $id);

            return response()->json($output)->withCookie(cookie('uploadedImages', $this->uploadedImages, $llct, null, null, false, false));

      // get uploaded video
      } elseif ($function == 'video') {

          $this->savedVideos = $request->cookie('uploadedVideos');
          $output = $this->uploadVideo($request, $context, $id);

          return response()->json($output)->withCookie(cookie('uploadedVideos', $this->uploadedVideos, $llct, null, null, false, false));

      } elseif ($function == 'image-delete') {

          $this->savedImages = $request->cookie('uploadedImages');
          $output = $this->deleteImage($request, $context, $id);

          return response()->json($output)->withCookie(cookie('uploadedImages', $this->uploadedImages, $llct, null, null, false, false));

      // return empty response
      } else {
            $output = '';
            return response()->json($output);
      }
    }

    /**
     * Uploads an Image
     * We will allow public upload (users that are not logged in) during the post-an-ad process but will sandbox the files
     *
     * @param object $request
     * @param string $context
     * @param string $id
     *
     * @return array
     */
    private function uploadImage($request, $context, $id = 'rubber-duck') {

      $user = auth()->user();
      $user = $user['attributes'];
      $uid  = $user['id'];

    /*  we can resize our image using the intervention package - for storing thumbnails on the fly
      $image = Image::make($request->file(‘image’)->resize(‘300′,’300’)->save(‘$filePathMedium’)->resize(‘100′,’100’)->save(‘$filePathThumb’); */

      if ($id == $uid || $context == 'public') {

            foreach ($_FILES AS $f) {
                  $file = $f;
            }

            // handle the filename
            $oldFileName      = $file['name'];
            $oldFileName      = str_replace(" ", "_", str_replace("-","_", $oldFileName));
            $ext              = explode(".", $oldFileName);
            $ext              = strtolower($ext[1]);
            $size             = $file['size'];
            $type             = false; //$file->getMimeType();
            $newFileName = md5(time() . $_SERVER['REMOTE_ADDR'] . $oldFileName) . "." . $ext;

            $date = date("Y-m-d h:i:s", time());

            if ($context == 'public') {
                // to the sandbox
                Storage::disk('sandbox')->put(
                    '/images/'.$newFileName,
                    file_get_contents($request->file('file')->getRealPath())
                );

            } else {
                Storage::disk('photos')->put(
                    '/'.$newFileName,
                    file_get_contents($request->file('file')->getRealPath())
                );

                DB::insert('insert into uploadLogs (file_user_id, file_type, file_mime, file_name, file_encrypted, file_size, file_created) values (?, ?, ?, ?, ?, ?, ?)',
                    [ $id, 'image', $type, $oldFileName, $newFileName, $size, $date ]
                );
            }

            $images = ((isset($this->savedImages) && !empty($this->savedImages)) ? $this->savedImages : false);
            $this->uploadedImages = ((strlen($images) >= 10) ? $images . "," . $newFileName : $newFileName);

            $output = array(
                  'file' => $newFileName
            );

            return array('record' => $output);
      }
      return array('error' => array( array('type' => 'uid'), array('message' => 'not found')));
    }

    /**
     * Removes and image from the designated area
     *
     * @param $request
     * @param $context
     * @param $id
     * @return array
     */
    private function deleteImage($request, $context, $id) {
        $user = auth()->user();
        $user = $user['attributes'];
        $uid  = $user['id'];

        if ($uid == $id || $context == 'public') {

            $file = $request->get('file');

            if ($file) {

                if ($context == 'public') {
                    Storage::disk('sandbox')->delete('/images/' . $file);
                    // remove from the cookie
                    $arr = [];
                    $images = explode(",", $this->savedImages);
                    foreach( $images as $image) {
                        if ($image == $file) continue;
                        $arr[] = $image;
                    }
                    $this->uploadedImages = implode(",", $arr);

                    return array('record' => 'success');
                }
            }
            return array('error' => array( array('type' => 'file'), array('message' => 'not found')));
        }
        return array('error' => array( array('type' => 'uid'), array('message' => 'not found')));
    }

    /**
     * Uploading a video - preliminary coding
     *
     * @param $request
     * @param $context
     * @param $id
     *
     * @return array
     */
    private function uploadVideo($request, $context, $id) {

      $user = auth()->user();
      $user = $user['attributes'];
      $uid  = $user['id'];

      if ($id == $uid) {

        $file = $request->file('file');

        $oldFileName      = $file->getClientOriginalName();
        $oldFileName      = str_replace(" ", "_", str_replace("-","_", $oldFileName));
        $ext              = $file->getClientOriginalExtension();
        $size             = $file->getClientSize();
        $type             = $file->getMimeType();

        $time             = time();

        $destinationPath  = storage_path() . '/videos/';

        if ($type == 'video/mpeg' || $type == 'video/avi') {

          // needs to be re formatted
          $tempPath = storage_path() . '/videos/uploads/';
          $request->file('file')->move($tempPath, $oldFileName);

          // dont add to database, just return the filename and begin the transcoding process using the VideoController with a nee AJAX request
          $output = array(

              'transcode' => 1,
              'file'      => $oldFileName,
              'size'      => $size,
              'type'      => $type

          );
          return array('records' => $output);

        } else {

          $newFileName      = md5($time.$_SERVER['REMOTE_ADDR'].$oldFileName) . "." . $ext;
          $request->file('file')->move($destinationPath, $newFileName);

        }

        $date = date("Y-m-d h:i:s", time());
        DB::insert('insert into uploadLogs (file_user_id, file_type, file_mime, file_name, file_encrypted, file_size, file_status, file_created) values (?, ?, ?, ?, ?, ?, ?)',
              [ $id, 'video', $type, $oldFileName, $newFileName, $size, 'available', $date ]
        );

        $videos = ((isset($this->savedVideos) && !empty($this->savedVideos)) ? $this->savedVideos : false);
        $this->uploadedVideos = ((strlen($videos) >= 10) ? $videos . "," . $newFileName : $newFileName);

        $output = array(

              'video' => $newFileName,
              'size'  => $size,
              'type'  => $type

        );

        return array('record' => $output);
      }
      return array('error' => array( array('type' => 'uid'), array('message' => 'not found')));
    }
}
