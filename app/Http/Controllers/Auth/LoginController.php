<?php

namespace App\Http\Controllers\Auth;

/**
 * @uses
 */
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

/**
 * Class LoginController
 * Todo: !! The login processes are still under review & development !!
 *
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/#/latest';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function login(Request $request)
    {
        // get the credentials
        $credentials = $request->only(['email', 'password']);

        // check the credentials
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['success' => false, 'error' => 'Unauthorized'], 401);
        }

        // get the user
        $user = auth()->user();

        // generate the user's token from Passport
        $token = $user->createToken('AdultMuse')->accessToken;

        // login the user
        Auth::login( $user );

        // check to see if they still have their 'app-member' cookie
        $appMember = $request->cookie('app-member');
        if (empty($appMember)) {
            // set the account member cookie
            $llct     = time()+60*60*24*180; // 180 days should be enough !!
            Cookie::queue('app-member', $user->id, $llct, false, false, false, false);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @param Request $request
     * @param $uid
     *
     * @return mixed
     */
    public function logout(Request $request, $uid)
    {
        $user = auth()->user();
        if ($uid == $user->id) {
            auth()->logout();
            return response()->json(['success' => true, 'message'=>'success']);
        }
        return response()->json(['success' => false, 'error' => 'failed'], 200);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getAuthUser(Request $request)
    {
        return response()->json(auth()->user());
    }

    /**
     * @param $token
     *
     * @return mixed
     */
    protected function respondWithToken($token)
    {
        $d7 = 60 * 60 * 27 * 28; // 28 days
        return response()->json([
            'success' => true,
            'uid' => Auth::id(),
            'token' => $token,
            'time' => time(),
            'token_type' => 'bearer',
            'expires_in' => time() + $d7
        ]);
//        return response()->json([
//            'success' => true,
//            'access_token' => $token,
//            'token_type' => 'bearer',
//            'expires_in' => auth()->factory()->getTTL() * 60
//        ]);
    }
}
