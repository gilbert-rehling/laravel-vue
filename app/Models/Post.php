<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use Notifiable;

    // define the table
    protected $table = 'public_posts';

    /**
     * Delivers the phone number to ClickSend notification for SMS notices
     *
     * @return mixed
     */
    public function routeNotificationForClickSend()
    {
        return $this->phone;
    }
}
