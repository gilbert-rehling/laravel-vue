<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model;
{
    // define the table
    protected $table = 'userAccounts';
}
