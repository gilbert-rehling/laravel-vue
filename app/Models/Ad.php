<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    // define the table
    protected $table = 'ad_listings';

    protected $primaryKey = 'ad_id';

    const CREATED_AT = 'ad_created_at';
    const UPDATED_AT = 'ad_updated_at';
}
