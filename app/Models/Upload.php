<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    // define the table
    protected $table = 'uploadLogs';
}
