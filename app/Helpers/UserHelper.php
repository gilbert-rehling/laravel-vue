<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserHelper
 *
 * @package App\Helpers
 */
class UserHelper
{
    /**
     * Returns a User instance
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private static function getUser()
    {
        $user = Auth::user();
        return $user;
    }

    /**
     * Returns a USer ID
     *
     * @return integer|null
     */
    public static function getId()
    {
        $user = self::getUser();
        if (isset($user->id) && $user->id >= 1) {
            return $user->id;
        }
        return null;
    }

    /**
     * Return the full name of the current user
     *
     * @return string
     */
    public static function getName()
    {
        $user = self::getUser();
        if ($user) {
            return $user->given_names . ' ' . $user->family_name;
        }
        return '';
    }

    /**
     * Returns the current user email address
     *
     * @return string
     */
    public static function getEmail()
    {
        $user = self::getUser();
        if ($user) {
            return $user->email;
        }
        return '';
    }

    /**
     * Returns the current user created data
     *
     * @return false|int|string
     */
    public static function getCreated()
    {
        $user = self::getUser();
        if ($user) {
            return strtotime($user->created_at);
        }
        return '';
    }

    /**
     * Returns a flag indicating if the current user has any ads
     *
     * @return bool
     */
    public static function hasAds( )
    {
        $user = self::getUser();
        if (isset($user)) {
            $ad = DB::table('ads_listings')->where('user_id', $user->id)->first();
            if (isset($ad) && isset($ad->id)) {
                return true;
            }
        }
        return false;
    }
}
