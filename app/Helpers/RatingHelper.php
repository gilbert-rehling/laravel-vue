<?php

namespace App\Helpers;

/**
 * Class RatingHelper
 *
 * @package App\Helpers
 */
class RatingHelper
{
    /**
     * Calculate the string to return based on the rating value provided
     *
     * @param integer $rating
     * @return string
     */
    private static function returnClass($rating)
    {
        if ($rating >= 50) {
            $rating = 100 - $rating;
            return 'rotate' . $rating . ' over';
        }
        if ($rating < 50) {
            return 'rotate' . $rating . ' under';
        }
        return '';
    }

    /**
     * Used to set a class in a template - only for Laravel rendered front-end
     *
     * @param null|integer $rating
     * @return string
     */
    public static function setClass($rating = null)
    {
        if (is_numeric($rating)) {
            return self::returnClass($rating);
        }
        return '';
    }
}
