# Laravel and Vue Work Sample  

This project is made publicly available to demonstrate some fundamental Laravel and Vue knowledge.

It's not a complete project and is still a work in progress. There are some blocks of code that may have been commented out for testing and brevity.

## The following points should ne noted:
- In order to maintain maximum backward compatibility there is no PHP 7 specific coding style
- The SMS ClickSend library was ported from a packaged version which was incompatible with current composer setting and was therefore implemented directly as a service
- The /resources/js/ defines a layout with logical separation of concerns
- Most of the Vue component contain *.scss snippets
- The /resources/sass/ contains the best practice layout for global SASS elements

## Here some small projects that were completed quite hastily
A modular Golang API:  
https://github.com/gilbert-rehling/go-api  

A Node JS emailing service API:  
https://github.com/gilbert-rehling/node-email-service-api

Both of the above projects were coded and tested/confirmed on my local Linux DEV environment